DROP TABLE IF EXISTS `applicationregister`;
CREATE TABLE `applicationregister` (
   `id` VARCHAR(50) NOT NULL PRIMARY KEY,
   `type` VARCHAR(50) NOT NULL,
   `parameters` TEXT NOT NULL,
   `userAgent` TEXT NOT NULL,
   `scopeId` INT(10),
   `startDate` VARCHAR(25) NOT NULL,
   `lastDate` VARCHAR(25) NOT NULL,
   `daysActive` INT(7) NOT NULL DEFAULT 1
)
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;