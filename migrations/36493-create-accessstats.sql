DROP TABLE IF EXISTS `accessstats`;
CREATE TABLE `accessstats` (
   `id` BIGINT NOT NULL PRIMARY KEY,
   `participantRole` VARCHAR(30) NOT NULL,
   `recordTimestamp` INT(10) NOT NULL,
   `recordLocation` VARCHAR(100) NOT NULL
)
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;