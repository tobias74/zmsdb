DROP TABLE IF EXISTS `process_archive`;
CREATE TABLE `process_archive` (
    `id` VARCHAR(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
    `processId` INT(5) NOT NULL ,
    `archiveId` INT(9) NOT NULL ,
    `status` ENUM("free","reserved","confirmed","queued","called","processing","pending","pickup","finished","missed","archived","deleted","anonymized","blocked","conflict") DEFAULT "free",
    `createIP` VARCHAR(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , 
	`createTimestamp` BIGINT(20) NOT NULL DEFAULT '0' , 
    `lastChange` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
    INDEX (`archiveId`, `processId`),
    PRIMARY KEY (`id`)
)

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;