<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb;

use BO\Zmsentities\AccessStats as Entity;
use BO\Zmsentities\Collection\AccessStatsList;

/**
 * @SuppressWarnings(ShortVariable)
 */
class AccessStats extends Base
{
    const DELETE_ENTRIES_AFTER_SECONDS = 1800;

    public function readList(): AccessStatsList
    {
        return $this->fetchList(
            (new Query\AccessStats(Query\Base::SELECT))->addEntityMapping(),
            new Entity(),
            new AccessStatsList()
        );
    }

    public function writeEntity(Entity $entity): ?Entity
    {
        if (!$entity->offsetExists('id')) {
            throw new \InvalidArgumentException('The AccessStats entity needs to be prepared with a trackable id');
        }

        if ($this->perform(Query\AccessStats::QUERY_INSERT, [
            'id'              => $entity['id'],
            'participantRole' => $entity['role'],
            'recordTimestamp' => $entity['lastActive'],
            'recordLocation'  => $entity['location'],
        ])) {
            $newId = $this->getWriter()->lastInsertId();

            $selectQuery = (new Query\AccessStats(Query\Base::SELECT))->addEntityMapping();
            $selectQuery->addPrimaryComparison($newId);

            return $this->fetchOne($selectQuery, new \BO\Zmsentities\AccessStats());
        }

        return null;
    }

    public function deleteEntity(int $entityId): void
    {
        $query = new Query\AccessStats(Query\Base::DELETE);
        $query->addPrimaryComparison($entityId);

        $this->deleteItem($query);
    }

    public function freshenList(): int
    {
        $query = new Query\AccessStats(Query\Base::DELETE);
        $query->addRecordTimestampComparison(self::DELETE_ENTRIES_AFTER_SECONDS);

        return $this->fetchAffected($query, $query->getParameters());
    }
}
