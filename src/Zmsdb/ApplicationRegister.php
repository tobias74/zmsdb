<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb;

use BO\Zmsentities\ApplicationRegister as ApplicationRegisterEntity;
use BO\Zmsentities\Collection\ApplicationRegisterList;

class ApplicationRegister extends Base
{
    public function readEntity($identifier): ?ApplicationRegisterEntity
    {
        $query = new Query\ApplicationRegister(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addPrimaryComparison($identifier);
        $entity = $this->fetchOne($query, new ApplicationRegisterEntity());

        return $entity->getId() === false ? null : $entity;
    }

    public function readList(): ApplicationRegisterList
    {
        return $this->fetchList(
            (new Query\ApplicationRegister(Query\Base::SELECT))->addEntityMapping(),
            new ApplicationRegisterEntity(),
            new ApplicationRegisterList()
        );
    }

    public function readListByScopeIds(array $scopeIds, ?string $restrictToType = null): ApplicationRegisterList
    {
        $query = (new Query\ApplicationRegister(Query\Base::SELECT))
            ->addEntityMapping()
            ->addScopeIdsCondition($scopeIds);

        if ($restrictToType) {
            $query->addTypeCondition($restrictToType);
        }

        return $this->fetchList(
            $query,
            new ApplicationRegisterEntity(),
            new ApplicationRegisterList()
        );
    }

    public function writeEntity(ApplicationRegisterEntity $entity): ?ApplicationRegisterEntity
    {
        if (!$entity->offsetExists('id')) {
            throw new \InvalidArgumentException('The ApplicationRegister entity needs to have a trackable id');
        }

        $insertQry = (new Query\ApplicationRegister(Query\Base::INSERT));
        if ($this->perform(Query\ApplicationRegister::QUERY_INSERT, $insertQry->reverseEntityMapping($entity))) {
            return $this->readEntity($entity->getId());
        }

        return null;
    }

    /**
     * @return int (number of deleted items/lines)
     */
    public function deleteOutdated(\DateTimeInterface $now): int
    {
        $deleteQuery = new Query\ApplicationRegister(Query\Base::DELETE);
        $deleteQuery->addOutdatedCondition($now);

        return $this->fetchAffected($deleteQuery, $deleteQuery->getParameters());
    }
}
