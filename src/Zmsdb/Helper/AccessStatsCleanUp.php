<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\AccessStats;
use Psr\Log\LoggerInterface;

class AccessStatsCleanUp
{
    /** @var LoggerInterface */
    private $logger;

    /** @var AccessStats  */
    private $repository;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->repository = new AccessStats();
    }

    public function startProcessing(): void
    {
        $countDeleted = $this->repository->freshenList();
        $this->logger->info($countDeleted . ' accessstats table lines have been deleted.');
    }
}
