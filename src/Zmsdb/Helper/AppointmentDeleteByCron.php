<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\ProcessArchive as ProcessArchiveRepo;
use BO\Zmsdb\ProcessStatusArchived as ProcessStatusRepository;

use Closure;
use DateTimeInterface;
use DateTimeImmutable;

/**
 * @codeCoverageIgnore
 */
class AppointmentDeleteByCron
{
    protected $verbose = false;

    protected $limit = 10000;

    protected $loopCount = 500;

    /** @var DateTimeInterface */
    protected $time;

    /** @var DateTimeInterface */
    protected $now;

    /**
     * List of possible appointment statuses to be considered when removing expired appointments.
     */
    protected $statuslist = [
        ProcessEntity::STATUS_BLOCKED,
        ProcessEntity::STATUS_DELETED,
        ProcessEntity::STATUS_CONFIRMED,
        ProcessEntity::STATUS_QUEUED,
        ProcessEntity::STATUS_CALLED,
        ProcessEntity::STATUS_MISSED,
        ProcessEntity::STATUS_PROCESSING,
    ];

     /**
     * List of possible appointment statuses where the appointment is given another status change
     * if it was an unattended appointment (confirmed, queued, called), then archived for statistical purposes
     * and only removed from the database afterward.
     */
    protected $archivelist = [
        ProcessEntity::STATUS_CONFIRMED,
        ProcessEntity::STATUS_QUEUED,
        ProcessEntity::STATUS_CALLED,
        ProcessEntity::STATUS_MISSED,
        ProcessEntity::STATUS_PROCESSING,
        ProcessEntity::STATUS_PENDING,
    ];

    /**
     * @var array
     */
    protected $count = [];

    public function __construct($timeIntervalDays, DateTimeInterface $now, $verbose = false)
    {
        $deleteInSeconds = (24 * 60 * 60) * $timeIntervalDays;
        $this->time = (new DateTimeImmutable())->setTimestamp($now->getTimestamp() - $deleteInSeconds);
        $this->now = $now;
        $this->verbose = $verbose;

        $this->log("INFO: Deleting appointments older than " . $this->time->format('c'));
    }

    protected function log($message): void
    {
        if ($this->verbose) {
            error_log($message);
        }
    }

    public function getCount(): array
    {
        return $this->count;
    }

    public function setLimit($limit): void
    {
        $this->limit = $limit;
    }

    public function setLoopCount($loopCount): void
    {
        $this->loopCount = $loopCount;
    }

    /**
     */
    public function startProcessing($commit, $pending = false): void
    {
        if ($pending) {
            $this->statuslist[] = ProcessEntity::STATUS_PENDING;
        }
        $this->count = array_fill_keys($this->statuslist, 0);
        $this->deleteExpiredProcesses($commit);
        $this->updateProcessArchive($commit);
        $this->log("\nSUMMARY: Deleted processes: ".var_export($this->count, true));
    }

    protected function deleteExpiredProcesses($commit): void
    {
        foreach ($this->statuslist as $status) {
            $this->log("\nDelete expired processes with status $status:");
            $count = $this->deleteByCallback($commit, function ($limit, $offset) use ($status) {
                $query = new ProcessRepository();
                return $query->readExpiredProcessListByStatus($this->time, $status, $limit, $offset, 1);
            });
            $this->count[$status] += $count;
        }
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    protected function updateProcessArchive($commit): void
    {
        // remove blocked entries
        $processArchiveList = (new ProcessArchiveRepo())->readListByStatus(ProcessEntity::STATUS_BLOCKED);
        foreach ($processArchiveList as $processArchive) {
            $this->log(
                "\nSet processArchive entries with status "
                . ProcessEntity::STATUS_BLOCKED . " to ". ProcessEntity::STATUS_DELETED
            );
            if ($commit) {
                (new ProcessArchiveRepo())
                    ->updateEntityStatus($processArchive->getId(), ProcessEntity::STATUS_DELETED);
            }
        }
    }

    protected function deleteByCallback($commit, Closure $callback): int
    {
        $processCount = 0;
        $startposition = 0;
        while ($processCount < $this->limit) {
            $processList = $callback($this->loopCount, $startposition);
            if (0 == $processList->count()) {
                break;
            }
            foreach ($processList as $process) {
                if (!$this->removeProcess($process, $commit, $processCount)) {
                    $startposition++;
                }
                $processCount++;
            }
        }
        return $processCount;
    }

    protected function removeProcess(ProcessEntity $process, $commit, $processCount): int
    {
        if (in_array($process->status, $this->statuslist)) {
            if (in_array($process->status, $this->archivelist)) {
                $this->log("INFO: $processCount. Archive $process");
                $process = $this->updateProcessStatus($process);
                if ($commit) {
                    $this->archiveProcess($process);
                }
            }
            $this->log("INFO: $processCount. Delete $process");
            if ($commit) {
                $this->deleteProcess($process);
                return 1;
            }
        } else {
            $this->log("INFO: Keep process $process");
        }
        return 0;
    }

    protected function updateProcessStatus(ProcessEntity $process): ProcessEntity
    {
        if (in_array($process->status, [
            ProcessEntity::STATUS_CONFIRMED,
            ProcessEntity::STATUS_QUEUED,
            ProcessEntity::STATUS_CALLED])
        ) {
            $process->status = 'missed';
        }
        return $process;
    }

    protected function archiveProcess(ProcessEntity $process): void
    {
        $archived = (new ProcessStatusRepository())->writeEntityFinished($process, $this->now);
        if ($archived) {
            $this->log("INFO: Archived with Status=$process->status and Id=" . $archived->archiveId);
        }
    }

    protected function deleteProcess(ProcessEntity $process): void
    {
        if ((new ProcessRepository())->writeDeletedEntity($process->id)) {
            $this->log("INFO: Process $process->id successfully removed");
        } else {
            $this->log("WARN: Could not remove process '$process->id'!");
        }
    }
}
