<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Log as LogRepository;
use DateTimeInterface;

/**
 * @codeCoverageIgnore
 */
class LogListDeleteByCron
{
    /** @var bool */
    protected $verbose = false;

    /** @var array */
    protected $countLogsByCreateTs = 0;

    /** @var array */
    protected $countLogsByExpiresOn = 0;

    /** @var DateTimeInterface */
    protected $now;

    public function __construct(DateTimeInterface $now, $verbose = false)
    {
        $this->now = $now;
        $this->verbose = $verbose;
    }

    protected function log($message): void
    {
        if ($this->verbose) {
            error_log($message);
        }
    }

    public function startProcessing($commit): void
    {
        $this->deleteLogsByCreateTimestamp($commit);
        $this->deleteLogsByExpiresOnDate($commit);

        $this->log("\nSUMMARY: Deleted expired log entries by create timestamp: ".
            var_export($this->countLogsByCreateTs, true));
        $this->log("SUMMARY: Deleted expired log entries by expires_on date: ".
            var_export($this->countLogsByExpiresOn, true));
    }

    protected function deleteLogsByCreateTimestamp($commit): void
    {
        $query = new LogRepository();
        if ($commit) {
            $this->countLogsByCreateTs = $query->deleteLogListByCreateTimestamp($this->now);
        }
    }

    protected function deleteLogsByExpiresOnDate($commit): void
    {
        $query = new LogRepository();
        if ($commit) {
            $this->countLogsByExpiresOn = $query->deleteLogListByExpiresOnDate($this->now);
        } else {
            $this->log("WARN: Could not remove expired logs!");
        }
    }

    public function getCountByExpireDate(): int
    {
        return $this->countLogsByExpiresOn;
    }

    public function getCountByCreateTs(): int
    {
        return $this->countLogsByCreateTs;
    }
}
