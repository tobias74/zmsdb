<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\MaintenanceSchedule as ScheduleRepository;
use BO\Zmsentities\Collection\MaintenanceScheduleList as ScheduleList;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Config;
use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;
use BO\Zmsentities\Helper\MaintenanceSchedule as EntityHelper;
use DateTimeInterface;

class MaintenanceSettlement
{
    /** @var ConfigRepository */
    private $configRepo;

    /** @var ScheduleRepository */
    private $scheduleRepo;

    public function __construct(?ConfigRepository $configRepo = null, ?ScheduleRepository $scheduleRepo = null)
    {
        $this->configRepo = $configRepo ?? new ConfigRepository();
        $this->scheduleRepo = $scheduleRepo ?? new ScheduleRepository();
    }

    /**
     * @return ScheduleEntity|null (Entity is not the one from MaintenanceScheduleList but has its ID)
     */
    public function getPlannedMaintenance(): ?ScheduleEntity
    {
        $configEntry = $this->configRepo->readProperty(Config::PROPERTY_MAINTENANCE_NEXT);

        if (!empty($configEntry)) {
            return new ScheduleEntity(json_decode($configEntry, true));
        }

        return null;
    }

    /**
     * @SuppressWarnings(CyclomaticComplexity)
     */
    public function scheduleNextMaintenance(
        ScheduleList $scheduleList,
        ?DateTimeInterface $nowTime = null
    ): void {
        $nowTime      = DateTime::create($nowTime ?? 'now');
        $inSchedule   = $this->getPlannedMaintenance();

        // the planned maintenance schedule entry has been deleted, so reset the plan
        if ($inSchedule
            && (!$scheduleList->getEntity($inSchedule->getId())
                || !EntityHelper::getNextRunTime($scheduleList->getEntity($inSchedule->getId()))
                || $inSchedule->getStartDateTime()->getTimestamp()
                    != EntityHelper::getNextRunTime($scheduleList->getEntity($inSchedule->getId()))->getTimestamp()
            )
        ) {
            $this->configRepo->replaceProperty(Config::PROPERTY_MAINTENANCE_NEXT, '');
            $inSchedule = null;
        }

        $nowTime = $this->getNowOrAfterMaintenance($nowTime, $scheduleList);
        $filteredList = $this->getSortedSuccessorArray($scheduleList, $nowTime);

        if (count($filteredList) === 0 && !$inSchedule) {
            return;
        } elseif ($inSchedule && count($filteredList) === 0) {
            $this->configRepo->replaceProperty(Config::PROPERTY_MAINTENANCE_NEXT, '');

            return;
        }

        /** @var ScheduleEntity $scheduleNext */
        $scheduleNext  = reset($filteredList);
        if ($inSchedule && $this->entriesEqual($inSchedule, $scheduleNext)) {
            return;
        }

        $scheduleNext['startDateTime'] = EntityHelper::getNextRunTime($scheduleNext, $nowTime);
        $reducedData = EntityHelper::getReducedArray($scheduleNext);

        $this->configRepo->replaceProperty(
            Config::PROPERTY_MAINTENANCE_NEXT,
            json_encode($reducedData),
            "Don't change this! This is the next scheduled maintenance data"
        );
    }

    /**
     * @return bool (true when maintenance was started, else false)
     */
    public function checkActivation(ScheduleList $scheduleList, ?DateTimeInterface $nowTime = null): bool
    {
        $nowTime = DateTime::create($nowTime ?? 'now');
        $plannedEntry = $this->getPlannedMaintenance();

        if ($plannedEntry
            && $plannedEntry->getStartDateTime()->getTimestamp() <= $nowTime->getTimestamp()
            && $scheduleList->getEntity($plannedEntry->getId())
        ) {
            /** @var ScheduleEntity $scheduleEntity */
            $scheduleEntity = $scheduleList->getEntity($plannedEntry->getId());
            $scheduleEntity->setActive(true);
            $scheduleEntity->setStartDateTime($plannedEntry->getStartDateTime());
            $this->scheduleRepo->updateEntity($scheduleEntity);
            $this->configRepo->replaceProperty(Config::PROPERTY_MAINTENANCE_NEXT, '');

            return true;
        }

        return false;
    }

    public function deactivateMaintenance(?ScheduleEntity $scheduleEntry = null): void
    {
        if ($scheduleEntry === null) {
            $scheduleList = $this->scheduleRepo->readList();
            $activeEntry  = $scheduleList->getActiveEntry();

            if ($activeEntry === null) {
                return;
            }
        } else {
            $activeEntry = $scheduleEntry;
        }

        $activeEntry->setActive(false);
        $activeEntry->setStartDateTime(null);
        $this->scheduleRepo->updateEntity($activeEntry);
    }

    public function getMaintenanceEndTime(ScheduleEntity $activeEntry): ?DateTimeInterface
    {
        $startTime = $activeEntry->getStartDateTime()->modify('-1 seconds');
        return  EntityHelper::getNextRunEnd($activeEntry, $startTime);
    }

    private function getNowOrAfterMaintenance(DateTimeInterface $nowTime, ScheduleList $list): DateTimeInterface
    {
        // target: no changes while maintenance is active, so search for the next after the current maintenance
        if ($list->getActiveEntry() !== null) {
            $endTime = $this->getMaintenanceEndTime($list->getActiveEntry())->modify('-1 second');
            $nowTime = $endTime->getTimestamp() > $nowTime->getTimestamp() ? $endTime : $nowTime;
        }

        return $nowTime;
    }

    private function getSortedSuccessorArray(ScheduleList $list, DateTimeInterface $nowTime): array
    {
        $sortedList = (array) ($list->sortByScheduleTime($nowTime));
        return array_filter($sortedList, function (ScheduleEntity $entry) use ($nowTime) {
            return EntityHelper::getNextRunTime($entry, $nowTime) !== null;
        });
    }
    
    public function entriesEqual(ScheduleEntity $entity1, ScheduleEntity $entity2): bool
    {
        return $entity1->getId() === $entity2->getId()
            && $entity1->getStartDateTime()
            && $entity2->getStartDateTime()
            && $entity1->getStartDateTime()->getTimestamp() === $entity2->getStartDateTime()->getTimestamp();
    }
}
