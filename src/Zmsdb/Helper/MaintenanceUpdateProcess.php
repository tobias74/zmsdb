<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\MaintenanceSchedule as ScheduleRepository;
use BO\Zmsentities\Helper\DateTime as DateTimeHelper;
use BO\Zmsentities\MaintenanceSchedule;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class MaintenanceUpdateProcess
{
    /** @var LoggerInterface  */
    private $logger;

    /** @var ConfigRepository */
    private $configRepo;

    /** @var ScheduleRepository */
    private $scheduleRepo;

    /** @var MaintenanceSettlement */
    private $settlement;

    public function __construct(
        ?LoggerInterface $logger = null,
        ?ConfigRepository $configRepository = null,
        ?ScheduleRepository $scheduleRepository = null
    ) {
        $this->logger  = $logger ?? new NullLogger();
        $this->configRepo = $configRepository ?? new ConfigRepository();
        $this->scheduleRepo = $scheduleRepository ?? new ScheduleRepository();
        $this->settlement = new MaintenanceSettlement($this->configRepo, $this->scheduleRepo);
    }

    public function startProcessing(?\DateTimeInterface $nowTime = null): void
    {
        $nowTime      = DateTimeHelper::create($nowTime ?? 'now');
        $scheduleList = $this->scheduleRepo->readList();

        $activeEntry  = $scheduleList->getActiveEntry();
        if ($activeEntry !== null) {
            $endTime = $this->settlement->getMaintenanceEndTime($activeEntry);
            if ($endTime === null || $endTime->getTimestamp() < $nowTime->getTimestamp()) {
                $this->settlement->deactivateMaintenance($activeEntry);
                $this->logger->info('The maintenance mode ended ' . $nowTime->format('Y-m-d H:i:s'));
            } elseif ($endTime) {
                $this->logger->debug('The maintenance mode is active until ' . $endTime->format('Y-m-d H:i:s'));
            }
        }

        if ($this->settlement->checkActivation($scheduleList, $nowTime)) {
            $this->logger->info('Starting maintenance mode at ' . $nowTime->format('Y-m-d H:i:s'));
        }

        $currentInSchedule   = $this->settlement->getPlannedMaintenance();

        $this->settlement->scheduleNextMaintenance($scheduleList, $nowTime);
        $this->logChange($currentInSchedule);
    }

    protected function logChange(?MaintenanceSchedule $before)
    {
        $afterCall = $this->settlement->getPlannedMaintenance();
        if ($afterCall === null) {
            $this->logger->debug('There is no (further) maintenance scheduled');
        } elseif ($before && $afterCall
            && $before->getId() === $afterCall->getId()
            && $before->getStartDateTime()->getTimestamp() === $afterCall->getStartDateTime()->getTimestamp()
        ) {
            $startTime = $afterCall->getStartDateTime()->format('Y-m-d H:i:s');
            $this->logger->debug('The maintenance schedule did not change and the scheduled time is ' . $startTime);
        } elseif ($afterCall) {
            $startTime = $afterCall->getStartDateTime()->format('Y-m-d H:i:s');
            $this->logger->info('The next maintenance has been scheduled and the start is ' . $startTime);
        }
    }
}
