<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Log;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Department as DepartmentRepository;
use BO\Zmsdb\Config as ConfigRepository;

use BO\Zmsentities\Collection\ProcessList as Collection;
use BO\Zmsentities\Mail;
use BO\Zmsentities\Process;

use BO\Mellon\Validator;
use BO\Mellon\Failure\Exception as MellonFailureException;
use DateTimeInterface;

class SendMailReminder
{
    /** @var DateTimeInterface */
    protected $dateTime;

    /** @var DateTimeInterface */
    protected $lastRun;

     /** @var int */
    protected $reminderInSeconds;

    /** @var bool */
    protected $verbose = false;

    /** @var bool */
    protected $isDryRun = false;

    /** @var int */
    protected $limit = 5000;

    /** @var int */
    protected $loopCount = 500;

    /** @var array */
    protected $processIdList = [];

    public function __construct(DateTimeInterface $now, $hours = 2, bool $verbose = false, bool $isDryRun = false)
    {
        $config = (new ConfigRepository())->readEntity();
        $configLimit = $config->getPreference('mailings', 'sqlMaxLimit');
        $configBatchSize = $config->getPreference('mailings', 'sqlBatchSize');
        $this->limit = ($configLimit) ? $configLimit : $this->limit;
        $this->loopCount  = ($configBatchSize) ? $configBatchSize : $this->loopCount;
        $this->dateTime = $now;
        $this->reminderInSeconds = (60 * 60) * $hours;
        $this->lastRun = (new MailRepository)->readReminderLastRun($now);
        $this->isDryRun = $isDryRun;
        $this->verbose = $verbose;
    }

    protected function log($message): bool
    {
        return $this->verbose && error_log($message);
    }

    public function getProcessIdList(): array
    {
        return $this->processIdList;
    }

    public function setLimit($limit): void
    {
        $this->limit = $limit;
    }

    public function setLoopCount($loopCount): void
    {
        $this->loopCount = $loopCount;
    }

    public function startProcessing(): void
    {
        if (! $this->isDryRun) {
            (new MailRepository)->writeReminderLastRun($this->dateTime);
        }
        $this->log(
            "\nINFO: Send email reminder (Limits: ".
            $this->limit ."|". $this->loopCount .") dependent on last run: ".
            $this->lastRun->format('Y-m-d H:i:s')
        );

        $this->writeMailReminderList();

        $this->log("\nINFO: Last run ". $this->dateTime->format('Y-m-d H:i:s'));
        $this->log("\nSUMMARY: Sent mail reminder: ".count($this->processIdList));
    }

    protected function writeMailReminderList(): void
    {
        // The offset parameter was removed here, because with each loop the processes are searched, which have not
        // been processed yet. An offset leads to the fact that with the renewed search the first results are skipped.
        $this->writeByCallback(function ($limit) {
            $processList = (new ProcessRepository)->readEmailReminderProcessListByInterval(
                $this->dateTime,
                $this->lastRun,
                $this->reminderInSeconds,
                $limit,
                null,
                2
            );
            return $processList;
        });
    }

    protected function writeByCallback(\Closure $callback): void
    {
        $processCount = 0;
        while ($processCount < $this->limit) {
            $this->log("***Stack count***: ".$processCount);
            $processList = $callback($this->loopCount);
            if (0 == $processList->count()) {
                break;
            }
            
            foreach ($processList as $process) {
                $processCount++;
                if (! in_array($process->getId(), $this->processIdList)) {
                    $this->writeReminder($process, $processCount);
                }
            }
        }
    }

    protected function writeReminder(Process $process, $processCount)
    {
        $department = (new DepartmentRepository())->readByScopeId($process->getScopeId(), 0);
        if ($department->hasMail() && $this->hasValidEmail($process)) {
            $this->processIdList[] = $process->getId();
            $config = (new ConfigRepository)->readEntity();
            $collection = $this->getProcessListOverview($process, $config);
            $entity = (new Mail)->toResolvedEntity($collection, $config, 'reminder');
            $this->log(
                "INFO: $processCount. Create mail with process ". $process->getId() .
                " - ". $entity->subject ." for ". $process->getFirstAppointment()
            );
            if ($this->isDryRun) {
                return;
            }
            $entity = (new MailRepository)->writeInQueue($entity, $this->dateTime);
            $logString = "mail#". (($entity->hasId()) ? $entity->getId() : 0) .
            " process#". $entity->getProcessId() .
            " subject: $entity->subject .";
            Log::writeLogEntry("Write Reminder (Mail::writeInQueue) $logString ", $process->getId());
            $this->log("INFO: Mail has been written in queue successfully with ID ". $entity->getId());
        }
    }

    protected function getProcessListOverview($process, $config): Collection
    {
        $collection  = (new Collection())->addEntity($process);
        if (in_array(getenv('ZMS_ENV'), explode(',', $config->getPreference('appointments', 'enableSummaryByMail')))) {
            $processList = (new ProcessRepository())->readListByMailAndStatusList(
                $process->getFirstClient()->email,
                [
                    Process::STATUS_CONFIRMED,
                    Process::STATUS_PICKUP
                ],
                2,
                50
            );
            //add list of found processes without the main process
            $collection->addList($processList->withOutProcessId($process->getId()));
        }
        $collection = $collection->withoutExpiredAppointmentDate($this->dateTime);
        return $collection;
    }

    protected function hasValidEmail(Process $process): bool
    {
        $client = $process->getFirstClient();
        $hasMail = $client->hasEmail();
        $email = null;
        try {
            $email = Validator::value(trim((string) $client->toProperty()->email->get()))
            ->isMail()
            ->hasDNS()
            ->assertValid()
            ->getValue();
        } catch (MellonFailureException $exception) {
            Log::writeLogEntry(
                "Write Reminder to ".
                trim((string) $client->toProperty()->email->get()) . ' - '. $exception->getMessage(),
                $process->getId()
            );
        }
        return ($hasMail && $email !== null);
    }
}
