<?php
namespace BO\Zmsdb;

use BO\Zmsentities\Log as Entity;
use BO\Zmsentities\Collection\LogList;
use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsdb\Process as ProcessRepository;

use DateTimeInterface;

/**
 * Logging for actions
 *
 */
class Log extends Base
{
    const PROCESS = 'buerger';
    const MIGRATION = 'migration';
    const ERROR = 'error';
    const PROCESS_GENERAL_EXPIRE_DURATION = 6;

    /**
     * @var string
     */
    public static $operator = 'lib';

    /**
     * write a new log entry to db by referenceId and type
     *
     * @param string                 $message
     * @param int                    $referenceId
     * @param string                 $type
     * @param DateTimeInterface|null $now
     *
     * @return bool
     */
    public static function writeLogEntry(
        string $message,
        int $referenceId,
        string $type = self::PROCESS,
        ?DateTimeInterface $now = null
    ): bool {
        $instance = new self();
        $query = new Query\Log(Query\Base::INSERT);
        $message .= " [" . static::$operator . "]";
        $query->addValues(
            [
                'message' => $message . static::backtraceLogEntry(),
                'reference_id' => $referenceId,
                'type' => $type,
                'expires_on' => self::getPreferredExpiresDate($type, $referenceId, $now),
            ]
        );
        return $instance->writeItem($query);
    }

    /**
     * Calculate the preferred expiration date of a log entry based on the appointment time or a custom time
     *
     * @param string                 $type
     * @param int                    $referenceId
     * @param DateTimeInterface|null $now
     *
     * @return string
     */
    private static function getPreferredExpiresDate(
        string $type,
        int $referenceId,
        ?DateTimeInterface $now = null
    ): string {
        if ($now) {
            return $now
                ->modify('+ '. self::PROCESS_GENERAL_EXPIRE_DURATION .'months')
                ->format('Y-m-d 23:59:59');
        }
        $processEntity = ($type == self::PROCESS) ?
            (new ProcessRepository())->readEntity($referenceId, new Helper\NoAuth(), 0) :
            new ProcessEntity();
        return $processEntity
            ->getArrivalTime()
            ->modify('+ '. self::PROCESS_GENERAL_EXPIRE_DURATION .'months')
            ->format('Y-m-d 23:59:59');
    }

    /**
     * read a log entry from db by referenceId and type
     *
     * @param int $referenceId
     *
     * @return LogList
     */
    public function readByProcessId(int $referenceId): LogList
    {
        $query = new Query\Log(Query\Base::SELECT);
        $query->addEntityMapping();
        $query->addConditionType();
        $query->addConditionReferenceId($referenceId);
        return new LogList($this->fetchList($query, new Entity()));
    }

    /**
     * removes log entries from depending on ts date period
     *
     * @param DateTimeInterface $now
     * @param string            $type
     *
     * @return int
     */
    public function deleteLogListByCreateTimestamp(
        DateTimeInterface $now,
        string $type = self::PROCESS
    ): int {
        $query = new Query\Log(Query\Base::DELETE);
        $query->addConditionByExpiredTs($now, $type);
        return $this->deleteItemWithCount($query);
    }

    /**
     * removes log entries from depending on expires_on date
     *
     * @param DateTimeInterface $now
     * @param string            $type
     *
     * @return int
     */
    public function deleteLogListByExpiresOnDate(
        DateTimeInterface $now,
        string $type = self::PROCESS
    ): int {
        $query = new Query\Log(Query\Base::DELETE);
        $query->addConditionByExpiresOnDate($now, $type);
        return $this->deleteItemWithCount($query);
    }

    /**
     * get log entry Backtrance
     */
    protected static function backtraceLogEntry(): string
    {
        $short = '';
        foreach (debug_backtrace() as $step) {
            if (isset($step['file'])
                && isset($step['line'])
                && !strpos($step['file'], 'Zmsdb')
            ) {
                return ' ('.basename($step['file'], '.php') .')';
            }
        }
        return $short;
    }
}
