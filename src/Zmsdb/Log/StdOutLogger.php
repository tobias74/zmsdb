<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Log;

class StdOutLogger extends \Psr\Log\Test\TestLogger
{
    public function log($level, $message, array $context = [])
    {
        echo $message . PHP_EOL;
        parent::log($level, $message, $context);
    }
}
