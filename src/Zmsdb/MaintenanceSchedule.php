<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb;

use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule as Entity;
use BO\Zmsentities\Collection\MaintenanceScheduleList;

/**
 * @SuppressWarnings(ShortVariable)
 */
class MaintenanceSchedule extends Base
{
    public function readEntity($id): ?Entity
    {
        $query = new Query\MaintenanceSchedule(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addPrimaryComparison($id);
        $entity = $this->fetchOne($query, new Entity());

        return $entity->getId() === false ? null : $entity;
    }

    public function readActiveEntity(): ?Entity
    {
        $query = new Query\MaintenanceSchedule(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addActiveConstraint();
        $entity = $this->fetchOne($query, new Entity());

        return $entity->getId() === false ? null : $entity;
    }

    public function readList(): MaintenanceScheduleList
    {
        return $this->fetchList(
            (new Query\MaintenanceSchedule(Query\Base::SELECT))->addEntityMapping(),
            new Entity(),
            new MaintenanceScheduleList()
        );
    }

    public function deleteEntity(int $id): void
    {
        $query = new Query\MaintenanceSchedule(Query\Base::DELETE);
        $query->addPrimaryComparison($id);

        $this->deleteItem($query);
    }

    public function writeEntity(Entity $entity): ?Entity
    {
        $query = new Query\MaintenanceSchedule(Query\Base::INSERT);
        $values = $query->reverseEntityMapping($entity);
        $query->addValues($values);

        $this->writeItem($query);
        $newId = $this->getWriter()->lastInsertId();

        return $this->readEntity($newId);
    }

    public function updateEntity(Entity $entity): ?Entity
    {
        $id     = $entity->getId();
        $query  = new Query\MaintenanceSchedule(Query\Base::UPDATE);
        $values = $query->reverseEntityMapping($entity);
        $query->addPrimaryComparison($id);
        $query->addValues($values);

        $this->writeItem($query);

        return $this->readEntity($id);
    }
}
