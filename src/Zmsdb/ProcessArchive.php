<?php
namespace BO\Zmsdb;

use BO\Mellon\Collection;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\ProcessArchive as Entity;
use BO\Zmsentities\Collection\ProcessArchiveList as ProcessArchiveCollection;
use BO\Zmsentities\Schema\Entity as SchemaEntity;

use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed as ProcessArchiveUpdateFailedException;

class ProcessArchive extends Base implements Interfaces\ResolveReferences
{
    /**
     * read a processArchive from db by primary
     *
     * @param string|null $primary
     *
     * @return null|Entity|SchemaEntity $entity
     */
    public function readEntity(string $primary = null): ?Entity
    {
        if (null === $primary) {
            return null;
        }
        $query = new Query\ProcessArchive(Query\Base::SELECT);
        $query->addEntityMapping()
            ->addConditionPrimary($primary);
        return $this->fetchOne($query, new Entity());
    }

    /**
     * read full list of processArchive from db
     *
     * @return ProcessArchiveCollection $collection
     */
    public function readList(): ProcessArchiveCollection
    {
        $collection = new ProcessArchiveCollection();
        $query = new Query\ProcessArchive(Query\Base::SELECT);
        $query->addEntityMapping();
        $result = $this->fetchList($query, new Entity());
        if ($result) {
            foreach ($result as $entity) {
                $collection->addEntity($entity);
            }
        }
        return $collection;
    }

    /**
     * read a list of processArchive from db by status
     *
     * @param string $status
     *
     * @return ProcessArchiveCollection $collection
     */
    public function readListByStatus(string $status): ProcessArchiveCollection
    {
        $collection = new ProcessArchiveCollection();
        $query = new Query\ProcessArchive(Query\Base::SELECT);
        $query->addEntityMapping()
            ->addConditionStatus($status);
        $result = $this->fetchList($query, new Entity());
        if ($result) {
            foreach ($result as $entity) {
                $collection->addEntity($entity);
            }
        }
        return $collection;
    }

    /**
     * Update a processArchive status
     *
     * @param string $processArchiveId
     * @param string $status
     *
     * @return Entity|SchemaEntity $entity
     * @throws ProcessArchiveUpdateFailedException
     */
    public function updateEntityStatus(String $processArchiveId, String $status): Entity
    {
        $query = new Query\ProcessArchive(Query\Base::UPDATE);
        $query->addConditionPrimary($processArchiveId);
        $query->addValues([
            'status' => $status,
        ]);
        $this->writeItem($query);

        $entity = $this->readEntity($processArchiveId);
        if (! $entity->getId()) {
            throw new ProcessArchiveUpdateFailedException();
        }

        return $entity;
    }

    /**
     * write a new processArchive entity to DB
     * @param ProcessEntity $process
     * @return Entity $entity
     */
    public function writeNewEntityByProcess(ProcessEntity $process): Entity
    {
        $query = new Query\ProcessArchive(Query\Base::INSERT);
        $query->addValues([
            'id' => $process->processArchiveId,
            'processId' => $process->getId(),
            'archiveId' => $process->archiveId,
            'createIP' => $process->createIP,
            'createTimestamp' => $process->createTimestamp,
            'status' => $process->status,
        ]);
        $this->writeItem($query);
        return $this->readEntity($process->processArchiveId);
    }

    /**
     * write archived process data to entity
     *
     * @param string $processArchiveId
     * @param int    $archiveId
     *
     * @return Entity $entity
     * @throws ProcessArchiveUpdateFailedException
     */
    public function writeArchivedEntity(string $processArchiveId, int $archiveId): Entity
    {
        $query = new Query\ProcessArchive(Query\Base::UPDATE);
        $query->addConditionPrimary($processArchiveId);
        $query->addValues([
            'archiveId' => $archiveId,
            'status' => ProcessEntity::STATUS_ARCHIVED,
        ]);
        $this->writeItem($query);
        $entity = $this->readEntity($processArchiveId);

        if (! $entity->getId()) {
            throw new ProcessArchiveUpdateFailedException();
        }
        return $entity;
    }

    /**
     * delete processArchive entity from DB
     * @param string $processArchiveId
     * @return boolean
     */
    public function writeDeleteEntity(String $processArchiveId): bool
    {
        $query = new Query\ProcessArchive(Query\Base::DELETE);
        $query->addConditionPrimary($processArchiveId);
        return $this->deleteItem($query);
    }
}
