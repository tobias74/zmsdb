<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Query;

use BO\Zmsentities\AccessStats as Entity;

class AccessStats extends Base
{
    public const TABLE = 'accessstats';
    public const ALIAS = 'accessStats';
    public const PRIMARY = 'id';

    public const QUERY_INSERT = '
        INSERT INTO accessstats (`id`, `participantRole`, `recordTimestamp`, `recordLocation`) 
            VALUES (:id, :participantRole, :recordTimestamp, :recordLocation) 
            ON DUPLICATE KEY UPDATE 
                participantRole = :participantRole, 
                recordTimestamp = :recordTimestamp, 
                recordLocation = :recordLocation;
    ';

    protected $resolveLevel = 0;

    public function addPrimaryComparison($primaryValue)
    {
        $this->query->where(static::PRIMARY, '=', $primaryValue);
    }

    public function getEntityMapping(): array
    {
        return [
            'id'         => self::ALIAS . '.' . static::PRIMARY,
            'role'       => self::ALIAS . '.participantRole',
            'lastActive' => self::ALIAS . '.recordTimestamp',
            'location'   => self::ALIAS . '.recordLocation',
        ];
    }

    public function reverseEntityMapping(Entity $entity): array
    {
        $values =  [
            'participantRole' => $entity['role'],
            'recordTimestamp' => (int) $entity['lastActive'],
            'recordLocation'  => $entity['location'],
        ];

        if ($entity->getId()) {
            $values['id'] = (int) $entity->getId();
        }

        return $values;
    }

    public function addRecordTimestampComparison($pastSeconds, $comparison = '>'): AccessStats
    {
        if (!in_array($comparison, [self::IS_EQ, self::IS_LT, self::IS_GT, self::IS_GTE, self::IS_LTE])) {
            throw new \InvalidArgumentException("The comparison '$comparison' is not implemented/allowed");
        }

        $this->query->where(
            self::expression(
                time() . ' - `'. self::ALIAS .'`.`recordTimestamp`'
            ),
            $comparison,
            $pastSeconds
        );

        return $this;
    }

    /**
     * @param array $data
     * @return array
     */
    public function postProcess($data): array
    {
        $data[static::PRIMARY] = (int) $data[static::PRIMARY];
        $data['location'] = $data['recordLocation'] ?? $data['location'] ?? '';
        $data['lastActive'] = (int) ($data['recordTimestamp'] ?? $data['lastActive'] ?? 0);
        $data['role'] = $data['participantRole'] ?? $data['role'] ?? '';

        unset($data['recordLocation']);
        unset($data['recordTimestamp']);
        unset($data['participantRole']);

        return $data;
    }
}
