<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Query;

use BO\Zmsentities\ApplicationRegister as ApplicationRegisterEntity;
use BO\Zmsentities\Helper\DateTime;

class ApplicationRegister extends Base
{
    public const TABLE = 'applicationregister';
    public const ALIAS = 'applicationRegister';
    public const PRIMARY = 'id';

    public const OUTDATED_AFTER = '-3 days';
    public const QUERY_INSERT = '
        INSERT INTO '. self::TABLE. ' 
            (`id`, `type`, `parameters`, `userAgent`, `scopeId`, `startDate`, `lastDate`, `daysActive`) 
            VALUES (
                :id,
                :type,
                COALESCE(:parameters, ""),
                COALESCE(:userAgent, ""),
                :scopeId,
                :startDate,
                :lastDate,
                1
            )
            ON DUPLICATE KEY UPDATE
                `type` = :type,
                `parameters` = COALESCE(:parameters, `parameters`),
                `userAgent` = COALESCE(:userAgent, `userAgent`),
                `scopeId` = COALESCE(:scopeId, `scopeId`),
                `daysActive` = daysActive + CAST(lastDate != :lastDate AS SIGNED INTEGER),
                `lastDate` = :lastDate;';

    protected $resolveLevel = 0;

    public function __construct($queryType, $prefix = '', $name = false, $resolveLevel = null)
    {
        parent::__construct($queryType, $prefix, $name, $resolveLevel);

        if ($queryType === self::SELECT) {
            /** @see \Solution10\SQL\Select::orderBy() */
            $this->query->orderBy([
                self::ALIAS . '.type' => 'ASC',
                self::ALIAS . '.startDate' => 'ASC',
            ]);
        }
    }

    public function getEntityMapping(): array
    {
        $mapping = [];
        $keys = array_keys((new ApplicationRegisterEntity())->getDefaults());
        $keys[] = 'id';

        foreach ($keys as $key) {
            $mapping[$key] = self::ALIAS . '.`' . $key . '`';
        }
        return $mapping;
    }

    public function reverseEntityMapping(ApplicationRegisterEntity $entity): array
    {
        return [
            'id' => $entity->id,
            'type' => $entity->type,
            'parameters' => $entity->parameters,
            'userAgent' => $entity->userAgent,
            'scopeId' => $entity->scopeId,
            'startDate' => $entity->startDate->format('Y-m-d H:i:s'),
            'lastDate' => $entity->lastDate->format('Y-m-d'),
            'daysActive' => $entity->daysActive,
        ];
    }

    public function addPrimaryComparison($primaryValue)
    {
        /** @see \Solution10\SQL\Select::where() */
        $this->query->where(static::PRIMARY, '=', $primaryValue);
    }

    public function addOutdatedCondition(
        \DateTimeInterface $now,
        $outdatedAfter = self::OUTDATED_AFTER
    ): ApplicationRegister {
        $now = DateTime::create($now);
        $this->query->where(self::ALIAS . '.lastDate', '<', $now->modify($outdatedAfter)->format('Y-m-d'));

        return $this;
    }

    public function addScopeIdsCondition(array $scopeIds): ApplicationRegister
    {
        $this->query->where(self::ALIAS . '.scopeId', 'IN', $scopeIds);

        return $this;
    }

    public function addTypeCondition(string $type): ApplicationRegister
    {
        $this->query->where(self::ALIAS . '.type', '=', $type);

        return $this;
    }
}
