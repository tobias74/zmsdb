<?php

namespace BO\Zmsdb\Query;

use BO\Zmsdb\Log as LogRepository;
use DateTimeInterface;

class Log extends Base
{
    /**
     * @var String TABLE mysql table reference
     */
    const TABLE = 'log';

    /**
     * @var String ALIAS mysql table alias reference
     */
    const ALIAS = 'log';

    /**
     * No resolving required here
     */
    protected $resolveLevel = 0;

    /**
     * @return array
     */
    public function getEntityMapping(): array
    {
        return [
            'type' => self::TABLE. '.type',
            'reference' => self::TABLE. '.reference_id',
            'message' => self::TABLE. '.message',
            'ts' => self::TABLE. '.ts',
            'expires_on' => self::TABLE. '.expires_on'
        ];
    }

    /**
     * @param string $referenceId
     *
     * @return $this
     */
    public function addConditionReferenceId(string $referenceId): self
    {
        $this->query->where(self::TABLE. '.reference_id', '=', $referenceId);
        $this->query->orderBy(self::TABLE. '.ts', 'DESC');
        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function addConditionType(string $type = LogRepository::PROCESS): self
    {
        $this->query->where(self::TABLE. '.type', '=', $type);
        return $this;
    }

    /**
     * @param DateTimeInterface $now
     * @param string            $type
     *
     * @return $this
     */
    public function addConditionByExpiresOnDate(
        DateTimeInterface $now,
        string $type = LogRepository::PROCESS
    ): self {
        $this->query->where(self::TABLE . '.type', '=', $type);
        $this->query->where(self::TABLE . '.expires_on', '<', $now->format('Y-m-d H:i:s'));
        $this->query->where(self::TABLE . '.expires_on', '!=', '0000-00-00 00:00:00');
        return $this;
    }

    /**
     * @param DateTimeInterface $now
     * @param string            $type
     *
     * @return self
     */
    public function addConditionByExpiredTs(
        DateTimeInterface $now,
        string $type = LogRepository::PROCESS
    ): self {
        $this->query->where(self::TABLE. '.type', '=', $type);
        $this->query->where(
            self::TABLE. '.ts',
            '<',
            $now
                ->modify('-'. LogRepository::PROCESS_GENERAL_EXPIRE_DURATION .' months')
                ->format('Y-m-d H:i:s')
        );
        $this->query->where(self::TABLE. '.expires_on', '=', '0000-00-00 00:00:00');
        return $this;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function postProcess($data)
    {
        $data[$this->getPrefixed('ts')] = strtotime($data[$this->getPrefixed('ts')]);
        $data[$this->getPrefixed('expires_on')] = strtotime($data[$this->getPrefixed('expires_on')]);
        return $data;
    }
}
