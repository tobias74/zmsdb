<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Query;

use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule as Entity;
use DateTimeInterface;

class MaintenanceSchedule extends Base
{
    public const TABLE = 'maintenanceschedule';
    public const ALIAS = 'maintenanceSchedule';
    public const PRIMARY = 'scheduleId';

    protected $resolveLevel = 0;

    public function addPrimaryComparison($primaryValue)
    {
        $this->query->where(static::PRIMARY, '=', $primaryValue);
    }

    public function addActiveConstraint()
    {
        $this->query->where('isActive', '=', 1);
    }

    public function getEntityMapping(): array
    {
        return [
            'id'                  => self::ALIAS . '.' . static::PRIMARY,
            'creatorId'           => self::ALIAS . '.creatorId',
            'creationDateTime'    => self::ALIAS . '.creationDateTime',
            'startDateTime'       => self::ALIAS . '.startDateTime',
            'isActive'            => self::ALIAS . '.isActive',
            'isRepetitive'        => self::ALIAS . '.isRepetitive',
            'timeString'          => self::ALIAS . '.scheduleTime',
            'duration'            => self::ALIAS . '.duration',
            'leadTime'            => self::ALIAS . '.leadTime',
            'area'                => self::ALIAS . '.affectedArea',
            'announcement'        => self::ALIAS . '.announcement',
            'documentBody'        => self::ALIAS . '.documentBody',
        ];
    }

    /**
     * values provided for write operations (creator and creation time always results from last modification)
     */
    public function reverseEntityMapping(Entity $entity): array
    {
        $start = $entity['startDateTime'];
        return [
            'creatorId'        => $entity['creatorId'],
            'creationDateTime' => $entity['creationDateTime']->format('Y-m-d H:i:s'),
            'startDateTime'    => $start instanceof DateTimeInterface ? $start->format('Y-m-d H:i:s') : null,
            'isActive'         => (int)$entity['isActive'],
            'isRepetitive'     => (int)$entity['isRepetitive'],
            'scheduleTime'     => $entity['timeString'],
            'duration'         => $entity['duration'],
            'leadTime'         => $entity['leadTime'],
            'affectedArea'     => $entity['area'],
            'announcement'     => $entity['announcement'],
            'documentBody'     => $entity['documentBody'],
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    public function postProcess($data): array
    {
        $data['id']               = (int)$data['id'];
        $data['creatorId']        = (int)$data['creatorId'];
        $data['creationDateTime'] = DateTime::create($data['creationDateTime']);
        $data['startDateTime']    = empty($data['startDateTime']) ? null : DateTime::create($data['startDateTime']);
        $data['isActive']         = (bool)$data['isActive'];
        $data['isRepetitive']     = (bool)$data['isRepetitive'];
        $data['duration']         = (int)$data['duration'];
        $data['leadTime']         = (int)$data['leadTime'];

        return $data;
    }
}
