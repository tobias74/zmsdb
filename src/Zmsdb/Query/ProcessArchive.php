<?php

namespace BO\Zmsdb\Query;

/**
 *
 * @SuppressWarnings(Methods)
 * @SuppressWarnings(Complexity)
 */
class ProcessArchive extends Base implements MappingInterface
{

    /**
     *
     * @var String TABLE mysql table reference
     */
    const TABLE = 'process_archive';
    const ALIAS = 'process_archive';

    public const PRIMARY = 'id';

    protected $resolveLevel = 0;

    public function getEntityMapping(): array
    {
        return [
            'id' => self::ALIAS .'.'.  static::PRIMARY,
            'processId' => self::ALIAS .'.processId',
            'archiveId' => self::ALIAS .'.archiveId',
            'createIP' => self::ALIAS .'.createIP',
            'createTimestamp' => self::ALIAS .'.createTimestamp',
            'lastChange' => self::expression("UNIX_TIMESTAMP(". self::ALIAS .".lastChange)"),
            'status' => self::ALIAS .'.status',
        ];
    }

    public function addConditionPrimary($primary): self
    {
        $this->query->where(self::ALIAS .'.'.  static::PRIMARY, '=', $primary);
        return $this;
    }

    public function addConditionStatus($status): self
    {
        $this->query->where(self::ALIAS .'.status', '=', $status);
        return $this;
    }
}
