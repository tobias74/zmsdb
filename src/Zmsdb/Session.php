<?php

namespace BO\Zmsdb;

use BO\Zmsentities\Collection\SessionList;
use BO\Zmsentities\Session as SessionEntity;
use BO\Zmsdb\Query\Base as DQI;

class Session extends Base
{
    /**
     * Fetch status from db
     *
     * @return SessionEntity
     */
    public function readEntity($sessionName, $sessionId)
    {
        $query = new Query\Session(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addConditionSessionId($sessionId)
            ->addConditionSessionName($sessionName);
        $session = $this->fetchOne($query, new SessionEntity());
        if ($session && ! $session->hasId()) {
            return null;
        }
        return $session;
    }

    public function updateEntity($session)
    {
        $query = Query\Session::QUERY_WRITE;
        $this->perform($query, array(
            $session['id'],
            $session['name'],
            json_encode($session['content'])
        ));
        $entity = $this->readEntity($session['name'], $session['id']);
        return $entity;
    }

    public function deleteEntity($sessionName, $sessionId)
    {
        $query = Query\Session::QUERY_DELETE;
        $result = $this->perform($query, array(
            $sessionId,
            $sessionName
        ));
        return ($result) ? true : false;
    }

    public function deleteByTimeInterval($sessionName, $deleteInSeconds)
    {
        $selectQuery = new Query\Session(Query\Base::SELECT);
        $selectQuery
            ->addEntityMapping()
            ->addConditionSessionName($sessionName)
            ->addConditionTsAgeComparison($deleteInSeconds, DQI::IS_GTE);
        $statement = $this->fetchStatement($selectQuery);
        while ($sessionData = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $sessionData = (new Query\Session(Query\Base::SELECT))->postProcessJoins($sessionData);
            $entity = new SessionEntity($sessionData);
            if ($entity instanceof SessionEntity) {
                $deleteQuery = new Query\Session(Query\Base::DELETE);
                $deleteQuery
                    ->addConditionSessionName($sessionName)
                    ->addConditionSessionId($entity->id);
                $this->deleteItem($deleteQuery);
            }
        }
    }

    public function readLastActiveList(
        int $maxAgeInSeconds,
        string $sessionName,
        ?string $type = 'reduced'
    ): SessionList {
        $selectQuery = new Query\Session(Query\Base::SELECT);
        $selectQuery
            ->addEntityMapping($type)
            ->addConditionSessionName($sessionName)
            ->addConditionTsAgeComparison($maxAgeInSeconds, Query\Base::IS_LTE);

        return $this->fetchList($selectQuery, new SessionEntity(), new SessionList());
    }
}
