<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\AccessStats as AccessStatsRepo;
use BO\Zmsentities\AccessStats as Entity;
use PHPUnit\Framework\TestCase;

class AccessStatsTest extends TestCase
{
    public function testRepository(): void
    {
        $repo = new AccessStatsRepo();
        $repo->freshenList();
        $lastCount = count($repo->readList());
        $insertedId = $this->testWriteEntity();
        $this->testFetchCollection($lastCount);
        $this->cleanUp($insertedId);
    }

    private function testWriteEntity(): int
    {
        $unsavedData = new Entity([
            'id'         => random_int(900000000000000000, PHP_INT_MAX),
            'role'       => 'citizen',
            'lastActive' => 2134567890,
            'location'   => 'zmsappointment',
        ]);

        $accessStatsRepo = new AccessStatsRepo();
        $createdEntity = $accessStatsRepo->writeEntity($unsavedData);

        self::assertNotNull($createdEntity);
        self::assertIsInt($createdEntity['id']);

        return $createdEntity['id'];
    }

    private function testFetchCollection(int $lastCount): void
    {
        $repo = new AccessStatsRepo();
        $list = $repo->readList();

        self::assertCount($lastCount + 1, $list);
    }

    private function cleanUp(int $insertedId)
    {
        $accessStatsRepo = new AccessStatsRepo();
        $accessStatsRepo->deleteEntity($insertedId);
    }
}
