<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\ApplicationRegister as EntityRepo;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\ApplicationRegister as Entity;

class ApplicationRegisterTest extends \BO\Zmsdb\Tests\Base
{
    /** @var EntityRepo */
    private $repo;

    public function testRepository(): void
    {
        $beforeTest = static::$now;
        $farFuture  = (DateTime::create($beforeTest))->modify('+10 years');
        $this->repo = new EntityRepo();

        static::$now = $farFuture;
            $this->repo->deleteOutdated(static::$now);
        static::$now = $beforeTest;

        $lastCount = count($this->repo->readList());
        $this->testWriteEntity();
        $this->testFetchCollection($lastCount);
        $this->testUpdateEntity();

        static::$now = $farFuture;
            $this->repo->deleteOutdated(static::$now);
        static::$now = $beforeTest;
    }

    private function testWriteEntity(): void
    {
        $unsavedData = new Entity([
            'id'         => 'UUID1',
            'type'       => 'ZMS2 calldisplay',
            'parameters' => 'collections[clusterlist]=109&template=defaultplatz&hmac=VQiP541jIHmP42tS',
            'userAgent'  => 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',
            'startDate'  => new \DateTime('2022-02-02 12:34:56'),
            'scopeId'    => 141,
        ]);

        $createdEntity = $this->repo->writeEntity($unsavedData);

        self::assertNotNull($createdEntity);
        self::assertSame('UUID1', $createdEntity['id']);
        self::assertSame('2022-02-02 12:34:56', $createdEntity['startDate']->format('Y-m-d H:i:s'));
    }

    private function testFetchCollection(int $lastCount): void
    {
        $list = $this->repo->readList();

        self::assertCount($lastCount + 1, $list);
    }
    
    private function testUpdateEntity(): void
    {
        $unsavedData = new Entity([
            'id'         => 'UUID1',
            'type'       => 'ZMS2 calldisplay',
            'parameters' => 'collections[scopelist]=141',
            'userAgent'  => 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',
            'scopeId'    => 141,
        ]);

        /** @var Entity $createdEntity */
        $createdEntity = $this->repo->writeEntity($unsavedData);
        self::assertSame(1, $createdEntity->daysActive);
        self::assertSame('collections[scopelist]=141', $createdEntity->parameters);

        static::$now = static::$now->modify('+1 day');

        $unsavedData = new Entity([
            'id'         => 'UUID1',
            'type'       => 'ZMS2 calldisplay',
            'userAgent'  => 'Firefox',
            'lastDate'   => static::$now,
        ]);

        $createdEntity = $this->repo->writeEntity($unsavedData);
        self::assertSame(2, $createdEntity->daysActive);
        self::assertSame('collections[scopelist]=141', $createdEntity->parameters);
        self::assertSame('Firefox', $createdEntity->userAgent);
        self::assertSame(141, $createdEntity->scopeId);
    }
}
