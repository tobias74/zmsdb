<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests\Helper;

use BO\Zmsdb\ApplicationRegister;
use BO\Zmsdb\Helper\ApplicationRegisterCleanUpByCron;
use BO\Zmsentities\ApplicationRegister as ApplicationRegisterEntity;
use BO\Zmsentities\Helper\DateTime;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;

class ApplicationRegisterCleanUpByCronTest extends TestCase
{
    protected static $now;

    public function setUp(): void
    {
        static::$now = new \DateTimeImmutable('2016-04-01 11:55:00');
    }

    public function testCleanUp()
    {
        $beforeTest = static::$now;
        $testTime = DateTime::create($beforeTest);

        $repo = new ApplicationRegister();
        $repo->writeEntity(new ApplicationRegisterEntity([
            'id' => 'abcd',
            'type' => 'test',
            'parameters' => 'test=true',
            'userAgent' => 'Unit Test',
            'scopeId' => 123,
            'startDate' => $testTime,
            'lastDate' => $testTime,
            'daysActive' => 1,
        ]));

        $logger  = new TestLogger();
        $cleanUp = new ApplicationRegisterCleanUpByCron($logger);

        $cleanUp->startProcessing(static::$now);

        $loggerEntry = reset($logger->records);

        self::assertStringContainsString('0 application register lines', $loggerEntry['message']);

        static::$now = $testTime->modify('+4 days');

        $logger  = new TestLogger();
        $cleanUp = new ApplicationRegisterCleanUpByCron($logger);

        $cleanUp->startProcessing(static::$now);

        $loggerEntry = reset($logger->records);

        self::assertStringContainsString('1 application register lines', $loggerEntry['message']);

        static::$now = $beforeTest;
    }
}
