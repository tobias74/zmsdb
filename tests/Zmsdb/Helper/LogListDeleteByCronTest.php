<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Helper\LogListDeleteByCron;
use BO\Zmsdb\Log as LogRepository;
use BO\Zmsentities\Helper\DateTime as DateTimeHelper;
use Exception;

class LogListDeleteByCronTest extends Base
{

    public function testConstructor()
    {
        $helper = new LogListDeleteByCron(static::$now, false);
        $this->assertInstanceOf(LogListDeleteByCron::class, $helper);
    }

    /**
     * @throws Exception
     */
    public function testStartProcessingByCron()
    {
        $now = DateTimeHelper::create('2016-04-12 23:59:59');
        $sqlContent = $this->readFixture('loglist.sql');
        (new LogRepository())->perform($sqlContent, []);
        $logList = (new LogRepository())->readByProcessId(195000);
        $this->assertEquals(10, $logList->count());

        $logList = (new LogRepository())->readByProcessId(220000);
        $this->assertEquals(10, $logList->count());

        $helper = new LogListDeleteByCron($now, false); // verbose
        $helper->startProcessing(true);
        $this->assertEquals(802, $helper->getCountByCreateTs());
        $this->assertEquals(802, $helper->getCountByExpireDate());

        $logList = (new LogRepository())->readByProcessId(195000);
        $this->assertEquals(6, $logList->count());

        $logList = (new LogRepository())->readByProcessId(220000);
        $this->assertEquals(6, $logList->count());
    }

    /**
     * @throws Exception
     */
    public function testDeleteAll()
    {
        $now = DateTimeHelper::create('2016-04-15 23:59:59');
        $sqlContent = $this->readFixture('loglist.sql');
        (new LogRepository())->perform($sqlContent, []);

        $helper = new LogListDeleteByCron($now, false); // verbose
        $helper->startProcessing(true);
        $this->assertEquals(2005, $helper->getCountByCreateTs());
        $this->assertEquals(2005, $helper->getCountByExpireDate());

        $logList = (new LogRepository())->readByProcessId(195000);
        $this->assertEquals(0, $logList->count());

        $logList = (new LogRepository())->readByProcessId(220000);
        $this->assertEquals(0, $logList->count());
    }

    /**
     * @throws Exception
     */
    public function testDeleteNone()
    {
        $now = DateTimeHelper::create('2016-04-10 23:59:59');
        $sqlContent = $this->readFixture('loglist.sql');
        (new LogRepository())->perform($sqlContent, []);

        $helper = new LogListDeleteByCron($now, false); // verbose
        $helper->startProcessing(true);
        $this->assertEquals(0, $helper->getCountByCreateTs());
        $this->assertEquals(0, $helper->getCountByExpireDate());

        $logList = (new LogRepository())->readByProcessId(195000);
        $this->assertEquals(10, $logList->count());

        $logList = (new LogRepository())->readByProcessId(220000);
        $this->assertEquals(10, $logList->count());
    }
}
