<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests\Helper;

use BO\Zmsdb\Helper\MaintenanceUpdateProcess;
use BO\Zmsdb\MaintenanceSchedule;
use BO\Zmsdb\Tests\Mockups\ConfigRepoWithProperties;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;
use BO\Zmsentities\Collection\MaintenanceScheduleList;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Log\Test\TestLogger;
use Prophecy\PhpUnit\ProphecyTrait;

class MaintenanceUpdateProcessTest extends TestCase
{
    use ProphecyTrait;

    public function testStartProcessing(): void
    {
        $scheduleNtt1 = new ScheduleEntity([
            'id'               => 1,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '*/30 * * * *',
            'duration'         => 15,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);

        $scheduleNtt2 = new ScheduleEntity([
            'id'               => 2,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => false,
            'timeString'       => '2021-12-21 12:50:00',
            'duration'         => 15,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);

        $scheduleNtt3 = new ScheduleEntity([
            'id'               => 3,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '*/20 * * * *',
            'duration'         => 10,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);

        $scheduleList = new MaintenanceScheduleList();
        $scheduleList->offsetSet(1, $scheduleNtt1);
        $scheduleList->offsetSet(2, $scheduleNtt2);
        $scheduleList->offsetSet(3, $scheduleNtt3);

        $configFake = new ConfigRepoWithProperties();
        $scheduleRepoProphecy = $this->prophesize(MaintenanceSchedule::class);
        $scheduleRepoProphecy
            ->readList()
            ->willReturn($scheduleList);

        $scheduleRepoProphecy
            ->updateEntity(Argument::type(ScheduleEntity::class))
            ->willReturn(new ScheduleEntity());

        $logger = new TestLogger();
        $nowTime = DateTime::create('2021-12-21 12:00:00');
        $testedService = new MaintenanceUpdateProcess($logger, $configFake, $scheduleRepoProphecy->reveal());

        $testedService->startProcessing($nowTime);

        $nowTime = $nowTime->modify('+5 minutes');
        $testedService->startProcessing($nowTime);

        for ($i = 0; $i < 8; $i++) {
            $nowTime = $nowTime->modify('+10 minutes');
            $testedService->startProcessing($nowTime);
        }

        $changedScheduleList = new MaintenanceScheduleList();
        $changedScheduleList->offsetSet(2, $scheduleNtt2);
        $scheduleRepoProphecy
            ->readList()
            ->willReturn($changedScheduleList);

        $nowTime = $nowTime->modify('+10 minutes');
        $testedService = new MaintenanceUpdateProcess($logger, $configFake, $scheduleRepoProphecy->reveal());
        $testedService->startProcessing($nowTime);

        $expected = [
            'The next maintenance has been scheduled and the start is 2021-12-21 12:20:00',
            'The maintenance schedule did not change and the scheduled time is 2021-12-21 12:20:00',
            'The maintenance schedule did not change and the scheduled time is 2021-12-21 12:20:00',
            'Starting maintenance mode at 2021-12-21 12:25:00',
            'The next maintenance has been scheduled and the start is 2021-12-21 12:30:00',
            'The maintenance mode ended 2021-12-21 12:35:00',
            'Starting maintenance mode at 2021-12-21 12:35:00',
            'The next maintenance has been scheduled and the start is 2021-12-21 12:50:00',
            'The maintenance mode is active until 2021-12-21 12:45:00',
            'The maintenance schedule did not change and the scheduled time is 2021-12-21 12:50:00',
            'The maintenance mode ended 2021-12-21 12:55:00',
            'Starting maintenance mode at 2021-12-21 12:55:00',
            'The next maintenance has been scheduled and the start is 2021-12-21 13:20:00',
            'The maintenance mode is active until 2021-12-21 13:05:00',
            'The maintenance schedule did not change and the scheduled time is 2021-12-21 13:20:00',
            'The maintenance mode ended 2021-12-21 13:15:00',
            'The maintenance schedule did not change and the scheduled time is 2021-12-21 13:20:00',
            'Starting maintenance mode at 2021-12-21 13:25:00',
            'The next maintenance has been scheduled and the start is 2021-12-21 13:30:00',
            'There is no (further) maintenance scheduled',
        ];

        self::assertSame($expected, array_column($logger->records, 'message'));
    }
}
