<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Helper\SendMailReminder;
use BO\Zmsdb\Process;
use BO\Zmsdb\Log as LogRepository;

class SendMailReminderTest extends Base
{

    public function testConstructor(): void
    {
        $helper = new SendMailReminder(static::$now);
        $this->assertInstanceOf(SendMailReminder::class, $helper);
    }

    public function testReminder2Hours(): void
    {
        $mailReminderDryRun = new SendMailReminder(static::$now, 2, false, false); // verbose
        $mailReminderDryRun->setLimit(10);
        $mailReminderDryRun->setLoopCount(10);
        $mailReminderDryRun->startProcessing();

        $this->assertEquals(10, count($mailReminderDryRun->getProcessIdList()));
        $this->assertEquals(55662, $mailReminderDryRun->getProcessIdList()[0]);

        $logList = (new LogRepository())->readByProcessId(55662);
        $this->assertEquals(55662, $logList[1]['reference']);
        $this->assertStringContainsString('Write Reminder (Mail::writeInQueue)', $logList[1]['message']);
        $this->assertStringNotContainsString('@', $logList[1]['message']);
    }

    public function testReminder24Hours(): void
    {
        $now = static::$now->modify('+1 days');
        $mailReminderDryRun = new SendMailReminder($now, 24, false, true); // verbose
        $mailReminderDryRun->setLimit(20);
        $mailReminderDryRun->setLoopCount(10);
        $mailReminderDryRun->startProcessing();

        $this->assertEquals(6, count($mailReminderDryRun->getProcessIdList()));
        $this->assertEquals(189682, $mailReminderDryRun->getProcessIdList()[0]);
    }

    public function testReminderWithoutInvalidMailDNS(): void
    {
        $query = new Process();
        // Use the ID of the process in the first test to check if it is processed with invalid email.
        $process = $query->readEntity(55662, '2d2c');
        $process->clients[0]['email'] = 'test.invalid.mail.com';
        $process = $query->updateEntity($process, static::$now);
        
        $mailReminderDryRun = new SendMailReminder(static::$now, 1, false, true); // verbose
        $mailReminderDryRun->setLimit(20);
        $mailReminderDryRun->setLoopCount(10);
        $mailReminderDryRun->startProcessing();

        $logList = (new LogRepository())->readByProcessId(55662);
        $this->assertEquals(55662, $logList[0]['reference']);
        $this->assertStringContainsString(
            'no valid DNS entry found',
            $logList[0]['message']
        );
        $this->assertEquals(9, count($mailReminderDryRun->getProcessIdList()));
        $this->assertTrue(55662 != $mailReminderDryRun->getProcessIdList()[0]);
        $this->assertTrue(103005 == $mailReminderDryRun->getProcessIdList()[0]);
    }

    public function testReminderWithoutInvalidMailString(): void
    {
        $query = new Process();
        // Use the ID of the process in the first test to check if it is processed with invalid email.
        $process = $query->readEntity(55662, '2d2c');
        $process->clients[0]['email'] = '1234567890';
        $process = $query->updateEntity($process, static::$now);
        
        $mailReminderDryRun = new SendMailReminder(static::$now, 1, false, true); // verbose
        $mailReminderDryRun->setLimit(20);
        $mailReminderDryRun->setLoopCount(10);
        $mailReminderDryRun->startProcessing();

        $logList = (new LogRepository())->readByProcessId(55662);
        $this->assertEquals(55662, $logList[0]['reference']);
        $this->assertStringContainsString(
            'no valid email',
            $logList[0]['message']
        );
        $this->assertEquals(9, count($mailReminderDryRun->getProcessIdList()));
        $this->assertTrue(55662 != $mailReminderDryRun->getProcessIdList()[0]);
        $this->assertTrue(103005 == $mailReminderDryRun->getProcessIdList()[0]);
    }
}
