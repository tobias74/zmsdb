<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\MaintenanceSchedule;
use BO\Zmsentities\Collection\MaintenanceScheduleList;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule as Entity;

class MaintenanceScheduleTest extends Base
{
    public function testRepository(): void
    {
        $testIds = $this->testWriteEntities();
        $readOne = $this->testReadEntity($testIds[1]);

        $this->testReadActiveEntity();
        $this->testUpdateEntity($readOne);
        $this->testReadList($testIds);
        $this->testDeleteEntity($testIds[1]);
    }

    private function testWriteEntities(): array
    {
        $testIds = [];
        $accessStatsRepo = new MaintenanceSchedule();
        $unsavedData = new Entity([
            'creatorId'           => 138,
            'isActive'            => false,
            'isRepetitive'        => false,
            'timeString'          => '2096-01-01 12:34:56',
            'duration'            => 60,
            'area'                => 'zms*',
            'documentBody'            => '<html><body><h1>Komm bitte später wieder.</h1></body></html>',
        ]);

        $createdEntity = $accessStatsRepo->writeEntity($unsavedData);

        self::assertIsInt($createdEntity['id']);
        self::assertGreaterThan(0, $createdEntity['id']);
        self::assertSame(138, $createdEntity['creatorId']);
        self::assertInstanceOf(DateTime::class, $createdEntity['creationDateTime']);
        self::assertNull($createdEntity['startDateTime']);
        self::assertSame(false, $createdEntity['isActive']);
        self::assertSame(false, $createdEntity['isRepetitive']);
        self::assertSame('2096-01-01 12:34:56', $createdEntity['timeString']);
        self::assertSame(60, $createdEntity['duration']);
        self::assertSame('zms*', $createdEntity['area']);
        self::assertStringContainsString('Komm bitte später wieder.', $createdEntity['documentBody']);

        $testIds[] = $createdEntity->getId();
        $unsavedData = new Entity([
            'creatorId'           => 138,
            'isActive'            => false,
            'isRepetitive'        => false,
            'timeString'          => '2098-01-02 12:34:56',
            'duration'            => 1200,
            'area'                => 'zmsadmin',
            'documentBody'            => '<html><body><h1>Komm bitte morgen wieder.</h1></body></html>',
        ]);

        $createdEntity = $accessStatsRepo->writeEntity($unsavedData);

        self::assertIsInt($createdEntity['id']);
        self::assertGreaterThan(0, $createdEntity['id']);

        $testIds[] = $createdEntity->getId();
        $unsavedData = new Entity([
            'creatorId'           => 138,
            'startDateTime'       => DateTime::create(),
            'isActive'            => true,
            'isRepetitive'        => true,
            'timeString'          => '5 0 * * 1-7',
            'duration'            => 1380,
            'area'                => 'zmsappointment',
            'documentBody'            => '<html><body><h1>23/7 Wartung</h1></body></html>',
        ]);

        $createdEntity = $accessStatsRepo->writeEntity($unsavedData);
        $testIds[] = $createdEntity->getId();

        self::assertIsInt($createdEntity['id']);
        self::assertGreaterThan(0, $createdEntity['id']);

        return $testIds;
    }

    private function testReadEntity(int $testId): Entity
    {
        $repo = new MaintenanceSchedule();
        $entity = $repo->readEntity($testId);

        self::assertIsInt($entity->getId());
        self::assertGreaterThan(0, $entity->getId());
        self::assertSame(138, $entity->getCreatorId());
        self::assertInstanceOf(\DateTimeInterface::class, $entity->getCreationDateTime());
        self::assertInstanceOf(\DateTimeInterface::class, $entity->getCreationDateTime());
        self::assertSame(false, $entity->isActive());
        self::assertSame(false, $entity->isRepetitive());
        self::assertSame('2098-01-02 12:34:56', $entity->getTimeString());
        self::assertSame(1200, $entity->getDuration());
        self::assertSame('zmsadmin', $entity['area']);
        self::assertStringContainsString('morgen', $entity['documentBody']);

        return $entity;
    }

    private function testReadActiveEntity(): void
    {
        $repo = new MaintenanceSchedule();
        $entity = $repo->readActiveEntity();

        self::assertIsInt($entity->getId());
        self::assertGreaterThan(0, $entity->getId());
    }

    private function testUpdateEntity(Entity $entity): void
    {
        $repo = new MaintenanceSchedule();

        $entity['creatorId'] = 5119;
        $entity['timeString'] = '2098-05-06 12:34:56';

        $nowTime   = DateTime::create()->format('Y-m-d H:i:s');
        $newEntity = $repo->updateEntity($entity);

        self::assertSame(5119, $newEntity['creatorId']);
        self::assertSame('2098-05-06 12:34:56', $newEntity['timeString']);
        self::assertSame($nowTime, $newEntity['creationDateTime']->format('Y-m-d H:i:s'));
    }

    private function testReadList(array $testIds): void
    {
        $repo = new MaintenanceSchedule();
        /** @var MaintenanceScheduleList $list */
        $list = $repo->readList();

        $activeEntry = $list->getActiveEntry();
        self::assertInstanceOf(Entity::class, $activeEntry);
        self::assertInstanceOf(\DateTimeInterface::class, $activeEntry->getStartDateTime());

        $returnId = function ($entry) {
            return $entry->getId();
        };
        $listIds = array_values(array_map($returnId, (array) $list));
        /*
         * since the test can use the same db as the local dev-environment,
         * the test need to work stable with other/former schedule entries too
         */
        foreach ($testIds as $entryId) {
            self::assertContains($entryId, $listIds);
        }

        $list->sortByScheduleTime();
        // array_values is needed to ensure a sequential array key order
        $listIds  = array_values(array_map($returnId, (array) $list));

        // test the new order within the sorted list
        self::assertTrue(array_search($testIds[1], $listIds) > array_search($testIds[0], $listIds));
        self::assertTrue(array_search($testIds[0], $listIds) > array_search($testIds[2], $listIds));
        self::assertTrue(array_search($testIds[1], $listIds) > array_search($testIds[2], $listIds));
    }

    private function testDeleteEntity(int $entryId): void
    {
        $repo = new MaintenanceSchedule();
        $oldCount = count($repo->readList());

        $repo->deleteEntity($entryId);

        self::assertCount($oldCount - 1, $repo->readList());
    }
}
