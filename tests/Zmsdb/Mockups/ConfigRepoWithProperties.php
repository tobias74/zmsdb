<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests\Mockups;

use BO\Zmsdb\Config;

class ConfigRepoWithProperties extends Config
{
    protected $memoryDB = [];

    public function readProperty($property, $forUpdate = false)
    {
        return isset($this->memoryDB[$property]) ? $this->memoryDB[$property] : false;
    }

    public function replaceProperty($property, $value, $description = '')
    {
        $this->memoryDB[$property] = $value;

        return true;
    }

    public function deleteProperty($property)
    {
        unset($this->memoryDB[$property]);
    }
}
