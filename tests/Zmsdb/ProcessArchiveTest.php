<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;
use BO\Zmsdb\Exception\Process\ProcessReserveFailed;
use BO\Zmsdb\Exception\Process\ProcessUpdateFailed;
use BO\Zmsdb\Exception\Useraccount\InvalidCredentials;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\ProcessStatusFree as ProcessFreeRepository;
use BO\Zmsdb\ProcessArchive as ProcessArchiveRepository;
use BO\Zmsdb\ProcessStatusArchived as ProcessFinishedRepository;
use BO\Zmsdb\Workstation as WorkstationRepository;
use BO\Zmsdb\Scope as ScopeRepository;
use BO\Zmsdb\Helper\NoAuth as NoAuthHelper;

/**
 * @SuppressWarnings(TooManyPublicMethods)
 * @SuppressWarnings(Coupling)
 *
 */
class ProcessArchiveTest extends Base
{
    protected $processInstance = null;

    protected $workstation = null;

    /**
     * @throws PDOFailed
     * @throws InvalidCredentials
     * @throws ProcessReserveFailed
     */
    public function setUp(): void
    {
        parent::setUp();
        $input = $this->getTestProcessEntity();
        $processRepository = new ProcessFreeRepository();
        $this->scope = (new ScopeRepository())->readEntity(141);
        $this->processInstance = $processRepository->writeEntityReserved($input, static::$now);
        $this->workstation = (new WorkstationRepository)->writeEntityLoginByName(
            'testadmin',
            '$2y$10$Pe2qgUAcFwvXYd2Zsc5VDurMQhMTdhpSSOgHGxuC3VWXERkNDhJCC',
            static::$now,
            2
        );
    }

    public function testWithoutPrimary()
    {
        $processArchiveRepo = new ProcessArchiveRepository();
        $processArchive = $processArchiveRepo->readEntity();
        $this->assertEquals(null, $processArchive);
    }
    
    public function test1ReserveStatus()
    {
        $processArchiveRepo = new ProcessArchiveRepository();
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_RESERVED, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_RESERVED, $processArchive->status);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function test2ConfirmedStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();

        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);

        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_CONFIRMED, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_CONFIRMED, $processArchive->status);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     * @throws ProcessUpdateFailed
     */
    public function test3CalledStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();

        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);
            $this->processInstance->queue['callTime'] = $this->processInstance->queue['arrivalTime'] + 3600;
            $this->processInstance = $processRepository->updateEntity($this->processInstance, $now);

        $this->workstation->process = (new WorkstationRepository())
            ->writeAssignedProcess($this->workstation, $this->processInstance, $now);

        $this->processInstance = $processRepository->readByWorkstation($this->workstation, 1);
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_CALLED, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_CALLED, $processArchive->status);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     * @throws ProcessUpdateFailed
     */
    public function test4ProcessingStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();

        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);
        $this->processInstance->queue['callTime'] = 1464350400;
        $this->processInstance = $processRepository->updateEntity($this->processInstance, $now);

        $this->workstation->process = (new WorkstationRepository())
            ->writeAssignedProcess($this->workstation, $this->processInstance, $now);

        $this->processInstance = $processRepository->readByWorkstation($this->workstation, 1);

        $this->processInstance->queue['callTime'] = $this->processInstance->queue['arrivalTime'] + 3600;
        $this->processInstance->status = ProcessEntity::STATUS_PROCESSING;
        $this->processInstance = $processRepository->updateEntity($this->processInstance, $now);

        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_PROCESSING, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_PROCESSING, $processArchive->status);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function test5FinishedStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();
        $processFinishedRepo = new ProcessFinishedRepository();

        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);

        $processFinishedRepo->writeEntityFinished($this->processInstance, $now);
        $processArchiveId = $this->processInstance->processArchiveId;

        $this->processInstance = $processRepository
            ->readEntity($this->processInstance->id, new NoAuthHelper, 0);

        $processArchive = $processArchiveRepo->readEntity($processArchiveId);

        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_BLOCKED, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_ARCHIVED, $processArchive->status);
        $this->assertEquals('', $this->processInstance->processArchiveId);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     * @throws ProcessUpdateFailed
     */
    public function test6MissedStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();
        
        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);

        $this->processInstance->status = ProcessEntity::STATUS_MISSED;
        $this->processInstance = $processRepository->updateEntity($this->processInstance, $now);

        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);

        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_MISSED, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_MISSED, $processArchive->status);
    }

    public function test7PendingStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();

        $this->processInstance = $processRepository->writeNewPickup($this->scope, $now);
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);

        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_PENDING, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_PENDING, $processArchive->status);
    }

    public function test8PickupStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();
        
        $this->processInstance = $processRepository->writeNewPickup($this->scope, $now);
        $this->processInstance->status = ProcessEntity::STATUS_PICKUP;
        $this->workstation->process = (new WorkstationRepository())
            ->writeAssignedProcess($this->workstation, $this->processInstance, $now);

        $this->processInstance = $processRepository->readByWorkstation($this->workstation, 1);
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);

        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_PICKUP, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_PICKUP, $processArchive->status);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function test9CanceledStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();
        $processFinishedRepo = new ProcessFinishedRepository();

        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);
        $archiveId = $this->processInstance->processArchiveId;

        $this->processInstance = $processFinishedRepo
            ->writeCanceledEntity($this->processInstance->id, $this->processInstance->authKey, $now);

        $processArchive = $processArchiveRepo->readEntity($archiveId);
        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(null, $this->processInstance->processArchiveId);
        $this->assertEquals(ProcessEntity::STATUS_DELETED, $this->processInstance->status);
        $this->assertEquals(ProcessEntity::STATUS_DELETED, $processArchive->status);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function test10BlockedStatus()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();
        $processFinishedRepo = new ProcessFinishedRepository();

        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);

        $archiveId = $this->processInstance->processArchiveId;

        $processFinishedRepo->writeBlockedEntity($this->processInstance);
        $this->processInstance = $processRepository->readEntity($this->processInstance->id, new NoAuthHelper, 0);
        $processArchive = $processArchiveRepo->readEntity($archiveId);

        $this->assertEntity("\\BO\\Zmsentities\\ProcessArchive", $processArchive);
        $this->assertEquals(ProcessEntity::STATUS_BLOCKED, $this->processInstance->status);
        $this->assertEquals(null, $this->processInstance->processArchiveId);
        $this->assertEquals(ProcessEntity::STATUS_BLOCKED, $processArchive->status);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     * @throws ProcessUpdateFailed
     */
    public function test11AllStatusSuccess()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();
        $processFinishedRepo = new ProcessFinishedRepository();

        //check reserved status
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        self::assertEquals(ProcessEntity::STATUS_RESERVED, $processArchive->status);

        //check confirmed status
        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        self::assertEquals(ProcessEntity::STATUS_CONFIRMED, $processArchive->status);

        //check called status
        $this->processInstance->queue['callTime'] = $this->processInstance->queue['arrivalTime'] + 3600;
        $this->processInstance = $processRepository->updateEntity($this->processInstance, $now);
        $this->workstation->process = (new WorkstationRepository())
            ->writeAssignedProcess($this->workstation, $this->processInstance, $now);
        $this->processInstance = $processRepository->readByWorkstation($this->workstation, 1);
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        self::assertEquals(ProcessEntity::STATUS_CALLED, $processArchive->status);

        //check confirmed status
        $this->processInstance->status = ProcessEntity::STATUS_PROCESSING;
        $this->processInstance = $processRepository->updateEntity($this->processInstance, $now);
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        self::assertEquals(ProcessEntity::STATUS_PROCESSING, $processArchive->status);

        //check finished
        $processFinishedRepo->writeEntityFinished($this->processInstance, $now);
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        self::assertEquals(ProcessEntity::STATUS_ARCHIVED, $processArchive->status);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function testDeleteEntity()
    {
        $now = static::$now;
        $processRepository = new ProcessRepository();
        $processArchiveRepo = new ProcessArchiveRepository();

        $this->processInstance = $processRepository
            ->updateProcessStatus($this->processInstance, ProcessEntity::STATUS_CONFIRMED, $now);

        $processArchiveRepo->writeDeleteEntity($this->processInstance->processArchiveId);
        $processArchive = $processArchiveRepo->readEntity($this->processInstance->processArchiveId);
        $this->processInstance = $processRepository->readEntity($this->processInstance->id, new NoAuthHelper, 0);

        $this->assertEquals('', $this->processInstance->processArchiveId);
        $this->assertEquals(0, $processArchive->processId);
        $this->assertEquals(ProcessEntity::STATUS_FREE, $processArchive->status);
    }

    /**
     * @SuppressWarnings(ExcessiveMethodLength)
     */
    public function getTestProcessEntity(): ProcessEntity
    {
        // https://localhost/terminvereinbarung/termin/time/1464339600/151/
        return new ProcessEntity(array(
            "amendment"=>"",
            "appointments"=>[
                [
                    "date"=>"1464339600", // 2016-05-27 11:00:00
                    "scope"=>[
                        "id"=>"151"
                    ],
                    "slotCount"=>"1"
                ]
            ],
            "scope"=>[
                "contact"=>[
                    "email"=>""
                ],
                "hint"=>"Bürgeramt MV ",
                "id"=>"151",
                "preferences"=>[
                    "appointment"=>[
                        "deallocationDuration"=>"5",
                        "endInDaysDefault"=>"60",
                        "multipleSlotsEnabled"=>"1",
                        "reservationDuration"=>"5",
                        "startInDaysDefault"=>"0",
                        "notificationHeadsUpEnabled"=>"1"
                    ],
                    "client"=>[
                        "alternateAppointmentUrl"=>"",
                        "amendmentActivated"=>"0",
                        "amendmentLabel"=>"",
                        "emailRequired"=>"1",
                        "telephoneActivated"=>"1",
                        "telephoneRequired"=>"1",
                        "emailFrom"=>"1"
                    ],
                    "notifications"=>[
                        "confirmationContent"=>"",
                        "enabled"=>"0",
                        "headsUpContent"=>"",
                        "headsUpTime"=>"0"
                    ],
                    "pickup"=>[
                        "alternateName"=>"Ausgabe",
                        "isDefault"=>"0"
                    ],
                    "queue"=>[
                        "callCountMax"=>"0",
                        "firstNumber"=>"1000",
                        "lastNumber"=>"1999",
                        "processingTimeAverage"=>"15",
                        "publishWaitingTimeEnabled"=>"1",
                        "statisticsEnabled"=>"1"
                    ],
                    "survey"=>[
                        "emailContent"=>"",
                        "enabled"=>"0",
                        "label"=>""
                    ],
                    "ticketprinter"=>[
                        "confirmationEnabled"=>"0",
                        "deactivatedText"=>"",
                        "notificationsAmendmentEnabled"=>"0",
                        "notificationsDelay"=>"0"
                    ],
                    "workstation"=>[
                        "emergencyEnabled"=>"0"
                    ]
                ],
                "shortName"=>"",
                "status"=>[
                    "emergency"=>[
                        "acceptedByWorkstation"=>"-1",
                        "activated"=>"0",
                        "calledByWorkstation"=>"-1"
                    ],
                    "queue"=>[
                        "ghostWorkstationCount"=>"-1",
                        "givenNumberCount"=>"11",
                        "lastGivenNumber"=>"1011",
                        "lastGivenNumberTimestamp"=>"1447925159"
                    ],
                    "ticketprinter"=>[
                        "deactivated"=>"1"
                    ]
                ],
                "department"=>[
                    "contact"=>[
                        "city"=>"Berlin",
                        "street"=>"Teichstr.",
                        "streetNumber"=>"1)",
                        "postalCode"=>"13407",
                        "region"=>"Berlin",
                        "country"=>"Germany",
                        "name"=>""
                    ],
                    "email"=>"buergeraemter@reinickendorf.berlin.de",
                    "id"=>"77",
                    "name"=>"Bürgeramt",
                    "preferences"=>[
                        "notifications"=>[
                            "enabled"=>null,
                            "identification"=>null,
                            "sendConfirmationEnabled"=>null,
                            "sendReminderEnabled"=>null
                        ]
                    ]
                ],
                "provider"=>[
                    "contact"=>[
                        "email"=>"buergeraemter@reinickendorf.berlin.de",
                        "city"=>"Berlin",
                        "country"=>"Germany",
                        "name"=>"Bürgeramt Märkisches Viertel",
                        "postalCode"=>"13435",
                        "region"=>"Berlin",
                        "street"=>"Wilhelmsruher Damm ",
                        "streetNumber"=>"142C"
                    ],
                    "source"=>"dldb",
                    "id"=>"122314",
                    "link"=>"https://service.berlin.de/standort/122314/",
                    "name"=>"Bürgeramt Märkisches Viertel"
                ]
            ],
            "clients"=>[
                [
                    "email"=>"max@service.berlin.de",
                    "emailSendCount"=>"1",
                    "familyName"=>"Max Mustermann",
                    "notificationsSendCount"=>"1",
                    "surveyAccepted"=>"0",
                    "telephone"=>"030 115"
                ]
            ],
            "createIP"=>"127.0.0.1",
            "createTimestamp" =>"1459028767",
            "queue"=>[
                "withAppointment" => 1,
                "arrivalTime" =>"1464339600",
                "callCount" => 0,
                "callTime" => "",
                "lastCallTime" => "0",
                "number" =>"0",
                "waitingTime" => 60,
                "reminderTimestamp" =>"0"
            ],
            "requests"=>[
                [
                    "id"=>"120686",
                    "link"=>"https://service.berlin.de/dienstleistung/120686/",
                    "name"=>"Anmeldung einer Wohnung",
                    "source"=>"dldb"
                ]
            ],
            "status"=>"free"
        ));
    }
}
