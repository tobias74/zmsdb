<?php

// @codingStandardsIgnoreFile
return array (
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 2,
      'intern' => 2,
      'callcenter' => 2,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '4',
      'intern' => '4',
      'callcenter' => '4',
      'type' => 'free',
    ),
  ),
  '30-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '30',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 72,
      'intern' => 72,
      'callcenter' => 72,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '92',
      'intern' => '92',
      'callcenter' => '92',
      'type' => 'free',
    ),
  ),
  '01-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '01',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '105',
      'intern' => '105',
      'callcenter' => '105',
      'type' => 'full',
    ),
  ),
  '04-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '04',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '05-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '05',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '123',
      'callcenter' => '123',
      'type' => 'full',
    ),
  ),
  '06-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '06',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '87',
      'intern' => '87',
      'callcenter' => '87',
      'type' => 'full',
    ),
  ),
  '07-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '07',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '08-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '08',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '105',
      'intern' => '105',
      'callcenter' => '105',
      'type' => 'full',
    ),
  ),
  '11-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '11',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '12-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '123',
      'callcenter' => '123',
      'type' => 'full',
    ),
  ),
  '13-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '13',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '87',
      'intern' => '87',
      'callcenter' => '87',
      'type' => 'full',
    ),
  ),
  '14-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '14',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '15-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '15',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '105',
      'intern' => '105',
      'callcenter' => '105',
      'type' => 'full',
    ),
  ),
  '18-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '18',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '19-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '123',
      'callcenter' => '123',
      'type' => 'full',
    ),
  ),
  '20-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '87',
      'intern' => '87',
      'callcenter' => '87',
      'type' => 'full',
    ),
  ),
  '21-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '21',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '22-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '22',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '105',
      'intern' => '105',
      'callcenter' => '105',
      'type' => 'full',
    ),
  ),
  '25-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '26-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '123',
      'callcenter' => '123',
      'type' => 'full',
    ),
  ),
  '27-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '27',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '87',
      'intern' => '87',
      'callcenter' => '87',
      'type' => 'full',
    ),
  ),
  '28-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '28',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '29-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '29',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '105',
      'intern' => '105',
      'callcenter' => '105',
      'type' => 'full',
    ),
  ),
  '02-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '02',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '03-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '03',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '123',
      'callcenter' => '123',
      'type' => 'full',
    ),
  ),
  '04-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '04',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '87',
      'intern' => '87',
      'callcenter' => '87',
      'type' => 'full',
    ),
  ),
  '06-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '06',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '105',
      'intern' => '105',
      'callcenter' => '105',
      'type' => 'full',
    ),
  ),
  '09-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '09',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '10-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '10',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '123',
      'callcenter' => '123',
      'type' => 'full',
    ),
  ),
  '11-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '11',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '87',
      'intern' => '87',
      'callcenter' => '87',
      'type' => 'full',
    ),
  ),
  '12-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '13-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '13',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '105',
      'intern' => '105',
      'callcenter' => '105',
      'type' => 'full',
    ),
  ),
  '17-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '17',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '123',
      'callcenter' => '123',
      'type' => 'full',
    ),
  ),
  '18-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '18',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '87',
      'intern' => '87',
      'callcenter' => '87',
      'type' => 'full',
    ),
  ),
  '19-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '141',
      'intern' => '141',
      'callcenter' => '141',
      'type' => 'full',
    ),
  ),
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '105',
      'intern' => '105',
      'callcenter' => '105',
      'type' => 'full',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '94',
      'intern' => '94',
      'callcenter' => '94',
      'type' => 'full',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '82',
      'intern' => '82',
      'callcenter' => '82',
      'type' => 'full',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '58',
      'intern' => '58',
      'callcenter' => '58',
      'type' => 'full',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '94',
      'intern' => '94',
      'callcenter' => '94',
      'type' => 'full',
    ),
  ),
);
