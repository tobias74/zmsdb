<?php

// @codingStandardsIgnoreFile
return array (
  '05-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '05',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 1,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '2',
      'intern' => '3',
      'callcenter' => '3',
      'type' => 'free',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 22,
      'intern' => 23,
      'callcenter' => 22,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '22',
      'intern' => '23',
      'callcenter' => '22',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 25,
      'intern' => 25,
      'callcenter' => 25,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '25',
      'intern' => '25',
      'callcenter' => '25',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 24,
      'intern' => 31,
      'callcenter' => 24,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '31',
      'callcenter' => '24',
      'type' => 'free',
    ),
  ),
  '01-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '01',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '60',
      'callcenter' => '60',
      'type' => 'full',
    ),
  ),
  '04-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '04',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '40',
      'intern' => '80',
      'callcenter' => '80',
      'type' => 'full',
    ),
  ),
  '06-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '06',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '52',
      'intern' => '80',
      'callcenter' => '80',
      'type' => 'full',
    ),
  ),
  '07-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '07',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '78',
      'intern' => '132',
      'callcenter' => '132',
      'type' => 'full',
    ),
  ),
  '11-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '11',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '40',
      'intern' => '80',
      'callcenter' => '80',
      'type' => 'full',
    ),
  ),
  '12-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '78',
      'intern' => '132',
      'callcenter' => '132',
      'type' => 'full',
    ),
  ),
  '13-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '13',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '28',
      'intern' => '28',
      'callcenter' => '28',
      'type' => 'full',
    ),
  ),
  '14-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '14',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '15-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '15',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '18-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '18',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '40',
      'intern' => '40',
      'callcenter' => '40',
      'type' => 'full',
    ),
  ),
  '19-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '20-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '28',
      'intern' => '28',
      'callcenter' => '28',
      'type' => 'full',
    ),
  ),
  '21-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '21',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '22-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '22',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '25-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '40',
      'intern' => '40',
      'callcenter' => '40',
      'type' => 'full',
    ),
  ),
  '26-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '27-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '27',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '28',
      'intern' => '28',
      'callcenter' => '28',
      'type' => 'full',
    ),
  ),
  '28-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '28',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '29-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '29',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '02-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '02',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '40',
      'intern' => '40',
      'callcenter' => '40',
      'type' => 'full',
    ),
  ),
  '03-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '03',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '04-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '04',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '28',
      'intern' => '28',
      'callcenter' => '28',
      'type' => 'full',
    ),
  ),
  '06-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '06',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '09-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '09',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '40',
      'intern' => '40',
      'callcenter' => '40',
      'type' => 'full',
    ),
  ),
  '10-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '10',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '11-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '11',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '28',
      'intern' => '28',
      'callcenter' => '28',
      'type' => 'full',
    ),
  ),
  '12-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '13-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '13',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '17-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '17',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '18-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '18',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '28',
      'intern' => '28',
      'callcenter' => '28',
      'type' => 'full',
    ),
  ),
  '19-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '42',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '40',
      'intern' => '40',
      'callcenter' => '40',
      'type' => 'full',
    ),
  ),
);
