<?php

// @codingStandardsIgnoreFile
return array (
  '06-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '06',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '3',
      'intern' => '4',
      'callcenter' => '3',
      'type' => 'free',
    ),
  ),
  '12-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 3,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '9',
      'intern' => '12',
      'callcenter' => '9',
      'type' => 'free',
    ),
  ),
  '13-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '13',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 2,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '6',
      'intern' => '8',
      'callcenter' => '6',
      'type' => 'free',
    ),
  ),
  '20-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '3',
      'intern' => '4',
      'callcenter' => '3',
      'type' => 'free',
    ),
  ),
  '22-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '22',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 2,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '6',
      'intern' => '8',
      'callcenter' => '6',
      'type' => 'free',
    ),
  ),
  '26-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 45,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '75',
      'intern' => '124',
      'callcenter' => '75',
      'type' => 'free',
    ),
  ),
  '27-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '27',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 36,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '108',
      'intern' => '144',
      'callcenter' => '108',
      'type' => 'free',
    ),
  ),
  '28-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '28',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 43,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '50',
      'intern' => '100',
      'callcenter' => '50',
      'type' => 'free',
    ),
  ),
  '29-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '29',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 38,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '114',
      'intern' => '152',
      'callcenter' => '114',
      'type' => 'free',
    ),
  ),
  '02-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '02',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 31,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '93',
      'intern' => '124',
      'callcenter' => '93',
      'type' => 'free',
    ),
  ),
  '03-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '03',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 57,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '99',
      'intern' => '156',
      'callcenter' => '99',
      'type' => 'free',
    ),
  ),
  '04-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '04',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 40,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '120',
      'intern' => '160',
      'callcenter' => '120',
      'type' => 'free',
    ),
  ),
  '06-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '06',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 40,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '40',
      'intern' => '80',
      'callcenter' => '40',
      'type' => 'free',
    ),
  ),
  '09-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '09',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 36,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '108',
      'intern' => '144',
      'callcenter' => '108',
      'type' => 'free',
    ),
  ),
  '10-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '10',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 53,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '96',
      'intern' => '152',
      'callcenter' => '96',
      'type' => 'free',
    ),
  ),
  '11-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '11',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 41,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '164',
      'callcenter' => '123',
      'type' => 'free',
    ),
  ),
  '12-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 67,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '70',
      'intern' => '140',
      'callcenter' => '70',
      'type' => 'free',
    ),
  ),
  '13-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '13',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 42,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'free',
    ),
  ),
  '17-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '17',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 56,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '96',
      'intern' => '152',
      'callcenter' => '96',
      'type' => 'free',
    ),
  ),
  '18-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '18',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 41,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '123',
      'intern' => '164',
      'callcenter' => '123',
      'type' => 'free',
    ),
  ),
  '19-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 82,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '84',
      'intern' => '168',
      'callcenter' => '84',
      'type' => 'free',
    ),
  ),
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 40,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '120',
      'intern' => '160',
      'callcenter' => '120',
      'type' => 'free',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 40,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '120',
      'intern' => '160',
      'callcenter' => '120',
      'type' => 'free',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 57,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '108',
      'intern' => '168',
      'callcenter' => '108',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 42,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 83,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '84',
      'intern' => '168',
      'callcenter' => '84',
      'type' => 'free',
    ),
  ),
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 7,
      'intern' => 49,
      'callcenter' => 7,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'free',
    ),
  ),
  '30-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '30',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 125,
      'intern' => 167,
      'callcenter' => 125,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'free',
    ),
  ),
  '01-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '01',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '168',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '04-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '04',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'full',
    ),
  ),
  '05-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '05',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '108',
      'intern' => '168',
      'callcenter' => '108',
      'type' => 'full',
    ),
  ),
  '07-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '07',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '84',
      'intern' => '168',
      'callcenter' => '84',
      'type' => 'full',
    ),
  ),
  '08-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '08',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'full',
    ),
  ),
  '11-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '11',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'full',
    ),
  ),
  '14-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '14',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '84',
      'intern' => '168',
      'callcenter' => '84',
      'type' => 'full',
    ),
  ),
  '15-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '15',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'full',
    ),
  ),
  '18-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '18',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'full',
    ),
  ),
  '19-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '108',
      'intern' => '168',
      'callcenter' => '108',
      'type' => 'full',
    ),
  ),
  '21-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '21',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '84',
      'intern' => '168',
      'callcenter' => '84',
      'type' => 'full',
    ),
  ),
  '25-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '126',
      'intern' => '168',
      'callcenter' => '126',
      'type' => 'full',
    ),
  ),
);
