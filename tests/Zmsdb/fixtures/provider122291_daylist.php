<?php

// @codingStandardsIgnoreFile
return array (
  '06-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '06',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 7,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '7',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '13-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '13',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 8,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '8',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '20-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '20',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 8,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '8',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '27-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '27',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 8,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '8',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '19-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '19',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 3,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '3',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 16,
      'callcenter' => 6,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '16',
      'callcenter' => '6',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 12,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '12',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 13,
      'callcenter' => 7,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '13',
      'callcenter' => '7',
      'type' => 'free',
    ),
  ),
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 5,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '5',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '01-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '01',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '19',
      'callcenter' => '19',
      'type' => 'full',
    ),
  ),
  '04-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '04',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '16',
      'intern' => '27',
      'callcenter' => '16',
      'type' => 'full',
    ),
  ),
  '05-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '05',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '27',
      'callcenter' => '10',
      'type' => 'full',
    ),
  ),
  '07-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '07',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '27',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '08-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '08',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '14',
      'type' => 'full',
    ),
  ),
  '11-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '11',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '22',
      'intern' => '22',
      'callcenter' => '22',
      'type' => 'full',
    ),
  ),
  '12-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '12',
      'intern' => '18',
      'callcenter' => '18',
      'type' => 'full',
    ),
  ),
  '14-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '14',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '15-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '15',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '14',
      'type' => 'full',
    ),
  ),
  '18-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '18',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '22',
      'intern' => '22',
      'callcenter' => '22',
      'type' => 'full',
    ),
  ),
  '19-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '12',
      'intern' => '12',
      'callcenter' => '12',
      'type' => 'full',
    ),
  ),
  '21-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '21',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '22-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '22',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '15',
      'intern' => '15',
      'callcenter' => '15',
      'type' => 'full',
    ),
  ),
  '25-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '8',
      'intern' => '23',
      'callcenter' => '19',
      'type' => 'full',
    ),
  ),
  '26-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '6',
      'intern' => '27',
      'callcenter' => '14',
      'type' => 'full',
    ),
  ),
  '28-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '28',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '10',
      'intern' => '27',
      'callcenter' => '24',
      'type' => 'full',
    ),
  ),
  '29-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '29',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '15',
      'intern' => '19',
      'callcenter' => '15',
      'type' => 'full',
    ),
  ),
  '02-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '02',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '27',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '03-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '03',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '27',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '04-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '04',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '4',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '06-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '06',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '09-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '09',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '27',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '10-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '10',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '6',
      'intern' => '27',
      'callcenter' => '14',
      'type' => 'full',
    ),
  ),
  '11-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '11',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '12-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '12',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '13-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '13',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '14',
      'type' => 'full',
    ),
  ),
  '17-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '17',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '27',
      'callcenter' => '10',
      'type' => 'full',
    ),
  ),
  '18-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '18',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '15',
      'callcenter' => '15',
      'type' => 'full',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '8',
      'intern' => '27',
      'callcenter' => '23',
      'type' => 'full',
    ),
  ),
);
