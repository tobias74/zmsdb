<?php

// @codingStandardsIgnoreFile
return array (
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 71,
      'callcenter' => 71,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '73',
      'intern' => '146',
      'callcenter' => '146',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '6',
      'intern' => '6',
      'callcenter' => '6',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 81,
      'callcenter' => 81,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '82',
      'intern' => '164',
      'callcenter' => '164',
      'type' => 'free',
    ),
  ),
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 32,
      'intern' => 32,
      'callcenter' => 32,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '56',
      'intern' => '56',
      'callcenter' => '56',
      'type' => 'free',
    ),
  ),
  '30-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '30',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 149,
      'intern' => 149,
      'callcenter' => 149,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '160',
      'intern' => '160',
      'callcenter' => '160',
      'type' => 'free',
    ),
  ),
);
