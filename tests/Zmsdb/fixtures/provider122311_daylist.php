<?php

// @codingStandardsIgnoreFile
return array (
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '6',
      'intern' => '6',
      'callcenter' => '6',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '2',
      'intern' => '2',
      'callcenter' => '2',
      'type' => 'free',
    ),
  ),
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 63,
      'intern' => 63,
      'callcenter' => 63,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '74',
      'intern' => '74',
      'callcenter' => '74',
      'type' => 'free',
    ),
  ),
  '30-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '30',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 131,
      'intern' => 131,
      'callcenter' => 131,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '166',
      'intern' => '166',
      'callcenter' => '166',
      'type' => 'free',
    ),
  ),
);
